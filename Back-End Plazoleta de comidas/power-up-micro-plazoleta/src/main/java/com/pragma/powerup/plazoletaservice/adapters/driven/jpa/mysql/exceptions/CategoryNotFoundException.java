package com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.exceptions;

public class CategoryNotFoundException extends RuntimeException{
    public CategoryNotFoundException() {
        super();
    }
}
