package com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.repositories;

import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.entity.CategoriaEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ICategoriaRepository extends JpaRepository<CategoriaEntity,Long> {
}
