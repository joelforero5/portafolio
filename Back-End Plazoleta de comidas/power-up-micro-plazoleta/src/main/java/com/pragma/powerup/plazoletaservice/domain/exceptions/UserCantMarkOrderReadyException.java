package com.pragma.powerup.plazoletaservice.domain.exceptions;

public class UserCantMarkOrderReadyException extends RuntimeException{
    public UserCantMarkOrderReadyException(){super();}
}
