package com.pragma.powerup.plazoletaservice.configuration.security.feign.client;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@Configuration
public class FeignClientConfig implements RequestInterceptor {
    @Override
    public void apply(RequestTemplate template) {
       /* String authToken = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiIxMjMiLCJyb2xlcyI6WyJST0xFX0FETUlOIl0sImlhdCI6MTY4NDUzNTM5OCw" +
                "iZXhwIjoxNjg1MTgzMzk4fQ.m_nbC4PGLYeYmR-3ffahHL_woLk-mi-BfNsfKrFZAKc";
        template.header("Authorization", "Bearer " + authToken);*/
        final String authorization = HttpHeaders.AUTHORIZATION;
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        String authorizationHeader = requestAttributes.getRequest().getHeader(HttpHeaders.AUTHORIZATION);
        template.header(authorization);
        template.header(authorization, authorizationHeader);
    }
}
