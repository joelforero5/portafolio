package com.pragma.powerup.plazoletaservice.domain.model;

public class EmpleadoRestaurante {
    private Long idEmpleado;
    private Restaurante idRestaurante;

    public EmpleadoRestaurante(Long idEmpleado, Restaurante idRestaurante) {
        this.idEmpleado = idEmpleado;
        this.idRestaurante = idRestaurante;
    }

    public Long getIdEmpleado() {
        return idEmpleado;
    }

    public void setIdEmpleado(Long idEmpleado) {
        this.idEmpleado = idEmpleado;
    }

    public Restaurante getIdRestaurante() {
        return idRestaurante;
    }

    public void setIdRestaurante(Restaurante idRestaurante) {
        this.idRestaurante = idRestaurante;
    }
}
