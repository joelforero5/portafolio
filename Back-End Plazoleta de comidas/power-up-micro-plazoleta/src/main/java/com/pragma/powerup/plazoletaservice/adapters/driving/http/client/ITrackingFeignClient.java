package com.pragma.powerup.plazoletaservice.adapters.driving.http.client;

import com.pragma.powerup.plazoletaservice.configuration.security.feign.client.FeignClientConfig;
import com.pragma.powerup.plazoletaservice.domain.model.Tracking;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;

@FeignClient(name = "tracking-service", url = "http://localhost:8095",configuration = FeignClientConfig.class)
public interface ITrackingFeignClient {
    @PostMapping("/tracking/create/")
    ResponseEntity<Map<String, String>> trackingOrder(@RequestBody Tracking tracking);
}
