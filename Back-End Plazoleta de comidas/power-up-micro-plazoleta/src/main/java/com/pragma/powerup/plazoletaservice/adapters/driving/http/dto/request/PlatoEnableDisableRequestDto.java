package com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
public class PlatoEnableDisableRequestDto {
    @Schema(accessMode = Schema.AccessMode.READ_ONLY)
    private Long id;
    @Schema(enumAsRef = true, allowableValues = {"true", "false"})
    private boolean activo;
}
