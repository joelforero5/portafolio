package com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.mappers;

import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.entity.PlatoEntity;
import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.entity.RestauranteEntity;
import com.pragma.powerup.plazoletaservice.domain.model.Plato;
import com.pragma.powerup.plazoletaservice.domain.model.Restaurante;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface IPlatoEntityMapper {
    IPlatoEntityMapper INSTANCE= Mappers.getMapper(IPlatoEntityMapper.class);
    PlatoEntity toEntity (Plato plato);
    Plato toPlato (PlatoEntity platoEntity);

    List<Plato> toPlatoList(List<PlatoEntity> platoEntityList);

    default Page<Plato> toPlatoPage(Page<PlatoEntity> page) {
        List<Plato> platoList = toPlatoList(page.getContent());
        return new PageImpl<>(platoList, page.getPageable(), page.getTotalElements());
    }
}
