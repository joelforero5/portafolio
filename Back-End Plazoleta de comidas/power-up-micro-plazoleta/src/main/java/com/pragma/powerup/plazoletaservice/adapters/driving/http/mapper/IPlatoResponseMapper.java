package com.pragma.powerup.plazoletaservice.adapters.driving.http.mapper;

import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.response.PlatoResponseDto;

import com.pragma.powerup.plazoletaservice.domain.model.Plato;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface IPlatoResponseMapper {

    PlatoResponseDto toDto(Plato plato);

    List<PlatoResponseDto> toDtoList(List<Plato> platoList);

    default Page<PlatoResponseDto> toDtoPage(Page<Plato> page) {
        List<PlatoResponseDto> responseDtos = toDtoList(page.getContent());
        return new PageImpl<>(responseDtos, page.getPageable(), page.getTotalElements());
    }
}
