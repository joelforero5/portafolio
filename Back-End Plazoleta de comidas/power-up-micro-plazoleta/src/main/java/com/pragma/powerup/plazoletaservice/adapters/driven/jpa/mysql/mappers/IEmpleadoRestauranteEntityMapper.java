package com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.mappers;

import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.entity.EmpleadoRestauranteEntity;
import com.pragma.powerup.plazoletaservice.domain.model.EmpleadoRestaurante;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface IEmpleadoRestauranteEntityMapper {
    EmpleadoRestaurante toEmpleadoRestaurante(EmpleadoRestauranteEntity empleadoRestauranteEntity);
    EmpleadoRestauranteEntity toEntity(EmpleadoRestaurante empleadoRestaurante);
}
