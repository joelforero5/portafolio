package com.pragma.powerup.plazoletaservice.domain.spi;

import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.entity.PedidoEntity;
import com.pragma.powerup.plazoletaservice.domain.model.Pedido;
import org.springframework.data.domain.Page;

public interface IPedidoPersistencePort {
    Long savePedido(Pedido order);

    void deleteOrderById(Long idOrder);

    Page<Pedido> getPedidos(int numeroPagina, int size, Long idEstado, Long idRestaurante, String campoOrdenar, String orden);

    void userCanCreateNewOrder(Long idUser);

    void asignarPedido(Long idPedido, Long idUser);

    PedidoEntity findPedidoById(Long idPedido);

    void pedidoListo(PedidoEntity pedido);

    void finalizarPedido(PedidoEntity pedido);

    void cancelarPedido(PedidoEntity pedido);

    PedidoEntity toPedidoEntity(Pedido pedido);

    Page<Pedido> getTiempoPedidos(int numeroPagina, int size, Long idRestaurante, String campoOrdenar, String orden);
    Page<Object[]> obtenerRankingEmployeesPorTiempoPromedio(int numeroPagina, int size, Long idRestaurante);
}
