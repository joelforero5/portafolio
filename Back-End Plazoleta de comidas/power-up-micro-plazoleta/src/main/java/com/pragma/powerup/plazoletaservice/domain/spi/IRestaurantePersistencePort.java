package com.pragma.powerup.plazoletaservice.domain.spi;

import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.response.RestauranteResponseDto;
import com.pragma.powerup.plazoletaservice.domain.model.EmpleadoRestaurante;
import com.pragma.powerup.plazoletaservice.domain.model.Pedido;
import com.pragma.powerup.plazoletaservice.domain.model.Restaurante;
import org.springframework.data.domain.Page;

import java.util.List;

public interface IRestaurantePersistencePort {

    void saveRestaurante(Restaurante restaurante);

    Page<Restaurante> getAllRestaurantesPage(int numeroPagina, int tamaño);

    void saveEmployee(EmpleadoRestaurante empleadoRestaurante);

    boolean existsByIdAndIdPropietario(Long idRestaurante, Long idUser);

    boolean existsEmployeeByIdAndIdRestaurante(Long idEmpleado, Restaurante idRestaurante);


}
