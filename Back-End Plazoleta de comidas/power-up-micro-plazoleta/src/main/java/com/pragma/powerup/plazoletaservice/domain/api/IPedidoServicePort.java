package com.pragma.powerup.plazoletaservice.domain.api;

import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.request.FinalizarPedidoReqDto;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.request.PedidoReqDto;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.response.RankingEmployeesResDto;
import com.pragma.powerup.plazoletaservice.domain.model.Pedido;
import org.springframework.data.domain.Page;

public interface IPedidoServicePort {
    void savePedido(PedidoReqDto pedidoReqDto,Long idUser);
    void asignarPedido(Long idPedido, Long idUser);

    Page<Pedido> getPedidos(int numeroPagina, int size, Long idEstado, Long idRestaurante,
                                  String campoOrdenar, String orden, Long authorization);

    void pedidoListo(Long idPedido, Long authorization);

    void finalizarPedido(FinalizarPedidoReqDto finalizarPedidoReqDto, Long authorization);

    void cancelarPedido(Long idPedido, Long authorization);

    Page<Pedido> getTiempoPedidos(int numeroPagina, int size, Long idRestaurante, String campoOrdenar, String orden, Long authorization);
    Page<RankingEmployeesResDto> getRankingEmployees(int numeroPagina, int size, Long idRestaurante, Long authorization);
}
