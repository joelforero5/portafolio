package com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.repositories;

import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.entity.CategoriaEntity;
import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.entity.PlatoEntity;
import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.entity.RestauranteEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IPlatoRepository extends JpaRepository<PlatoEntity,Long> {
    Page<PlatoEntity> findAllByIdRestauranteAndActivo(RestauranteEntity idRestaurante,boolean activo, Pageable pageable);
    Page<PlatoEntity> findAllByIdRestauranteAndIdCategoriaAndActivo(RestauranteEntity idRestaurante, CategoriaEntity idCategoria, boolean activo , Pageable pageable);
    boolean existsByIdAndIdRestaurante(Long id, RestauranteEntity idRestaurante);
}
