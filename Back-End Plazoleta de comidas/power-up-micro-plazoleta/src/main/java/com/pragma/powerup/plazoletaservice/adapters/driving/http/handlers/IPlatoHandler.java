package com.pragma.powerup.plazoletaservice.adapters.driving.http.handlers;

import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.request.PlatoEnableDisableRequestDto;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.request.PlatoRequestDto;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.request.PlatoUpdateReqDto;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.response.PlatoResponseDto;
import org.springframework.data.domain.Page;


public interface IPlatoHandler {
    void savePlato(PlatoRequestDto platoRequestDto);
    void updatePlato(PlatoUpdateReqDto platoUpdateReqDto);
    void updatePlatoActivo(PlatoEnableDisableRequestDto platoEnableDisableRequestDto);

    Page<PlatoResponseDto> getAllPlatos(int numeroPagina, int size, Long idCategoriaOrdenar,
                                        Long idRestaurante,String campoOrdenar,String orden);
}
