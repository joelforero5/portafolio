package com.pragma.powerup.plazoletaservice.adapters.driving.http.client.service;


public interface IUserClientService {
    Boolean existsOwner(Long id);
    Boolean existsClient(Long id);
    Boolean existsEmployee(Long id);

}
