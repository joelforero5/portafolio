package com.pragma.powerup.plazoletaservice.adapters.driving.http.controller;

import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.request.EmployeeRequestDto;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.request.RestauranteRequestDto;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.response.RankingEmployeesResDto;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.response.RestauranteResponseDto;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.handlers.IRestauranteHandler;
import com.pragma.powerup.plazoletaservice.configuration.Constants;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.Map;

@RestController
@RequestMapping("/restaurante")
@RequiredArgsConstructor
@SecurityRequirement(name = "jwt")
public class RestauranteRestController {
    private final IRestauranteHandler restauranteHandler;

    @Operation(summary = "Add a new Restaurante",
            responses = {
                    @ApiResponse(responseCode = "201", description = "Restaurante created",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Map"))),
                    @ApiResponse(responseCode = "409", description = "Restaurante already exists",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Error"))),
                    @ApiResponse(responseCode = "403", description = "Restaurante not allowed for owner creation",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Error")))})
    @PostMapping("/save")
    //@PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Map<String, String>> saveRestaurante(@Valid @RequestBody RestauranteRequestDto restauranteRequestDto) {
        restauranteHandler.saveRestaurante(restauranteRequestDto);
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(Collections.singletonMap(Constants.RESPONSE_MESSAGE_KEY, Constants.RESTAURANTE_CREATED_MESSAGE));
    }

    @Operation(summary = "Get all restaurantes",
            responses = {
                    @ApiResponse(responseCode = "200", description = "All Restaurantes returned"),
                    @ApiResponse(responseCode = "404", description = "Restaurantes not found ",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Error")))})
    @GetMapping("/get-restaurantes")
    public ResponseEntity<Page<RestauranteResponseDto>> getAllRestaurantes(@RequestParam int numeroPagina, @RequestParam int size) {
        return ResponseEntity.ok(restauranteHandler.getAllRestaurantes(numeroPagina,size));
    }

    @Operation(summary = "Add a new employee",
            responses = {
                    @ApiResponse(responseCode = "201", description = "owner employee",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Map"))),
                    @ApiResponse(responseCode = "409", description = "employee already exists",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Error"))),
                    @ApiResponse(responseCode = "403", description = "Role not allowed for employee creation",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Error")))})
    @PostMapping("/employee")
    public ResponseEntity<Map<String, String>> saveEmployee(@Valid @RequestBody EmployeeRequestDto employeeRequestDto) {
        restauranteHandler.saveEmployee(employeeRequestDto);
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(Collections.singletonMap(Constants.RESPONSE_MESSAGE_KEY, Constants.EMPLOYEE_CREATED_MESSAGE));
    }
}
