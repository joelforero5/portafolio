package com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.adapter;

import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.entity.CategoriaEntity;
import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.entity.PlatoEntity;
import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.entity.RestauranteEntity;
import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.exceptions.CategoryNotFoundException;
import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.exceptions.DishNotFoundException;
import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.exceptions.RestauranteNotFoundException;
import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.exceptions.UnauthorizedUserException;
import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.mappers.IPlatoEntityMapper;
import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.mappers.IRestauranteEntityMapper;
import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.repositories.ICategoriaRepository;
import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.repositories.IPlatoRepository;
import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.repositories.IRestauranteRepository;
import com.pragma.powerup.plazoletaservice.domain.model.Plato;
import com.pragma.powerup.plazoletaservice.domain.model.Restaurante;
import com.pragma.powerup.plazoletaservice.domain.spi.IPlatoPersistencePort;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@RequiredArgsConstructor
@Transactional
public class PlatoMysqlAdapter implements IPlatoPersistencePort {
    private final IPlatoRepository platoRepository;
    private  final IPlatoEntityMapper platoEntityMapper;
    private  final ICategoriaRepository categoriaRepository;
    private final IRestauranteRepository restauranteRepository;
    private final IRestauranteEntityMapper restauranteEntityMapper;

    @Override
    public void savePlato(Plato plato,Long idUser) {
        restauranteRepository.findByIdAndIdPropietario(plato.getIdRestaurante().getId(),idUser).orElseThrow(UnauthorizedUserException::new);
        categoriaRepository.findById(plato.getIdCategoria().getId()).orElseThrow(CategoryNotFoundException::new);
        PlatoEntity platoEntity = platoEntityMapper.toEntity(plato);
        platoRepository.save(platoEntity);
    }

    @Override
    public void updatePlato(Plato plato,Long idUser) {
        PlatoEntity platoActualizar = platoRepository.findById(plato.getId()).orElseThrow(DishNotFoundException::new);
        if(!platoActualizar.getIdRestaurante().getIdPropietario().equals(idUser)){
            throw new UnauthorizedUserException();
        }
        platoActualizar.setPrecio(plato.getPrecio());
        platoActualizar.setDescripcion(plato.getDescripcion());
        platoRepository.save(platoActualizar);

    }

    @Override
    public void updatePlatoActivo(Plato plato, Long idUser) {
        PlatoEntity platoEntity =platoRepository.findById(plato.getId()).orElseThrow(DishNotFoundException::new);
        if (!platoEntity.getIdRestaurante().getIdPropietario().equals(idUser)){
            throw new UnauthorizedUserException();
        }
        platoEntity.setActivo(plato.getActivo());
        platoRepository.save(platoEntity);
    }

    @Override
    public Page<Plato> getAllPlatos(int numeroPagina, int size, Long idCategoriaOrdenar, Long idRestaurante,String campoOrdenar,String orden) {
        Sort sort = Sort.by(orden.equalsIgnoreCase("desc")?Sort.Direction.DESC : Sort.Direction.ASC,campoOrdenar);
        Pageable pageable = PageRequest.of(numeroPagina,size,sort);
        CategoriaEntity categoriaEntity = new CategoriaEntity();
        categoriaEntity.setId(idCategoriaOrdenar);
        Page<PlatoEntity> platoEntityPage;
        RestauranteEntity restaurante = restauranteRepository.findById(idRestaurante).orElseThrow(RestauranteNotFoundException::new);
        if (idCategoriaOrdenar==null){
           platoEntityPage = platoRepository.findAllByIdRestauranteAndActivo(restaurante,true,pageable);
        }else{
           CategoriaEntity categoria = categoriaRepository.findById(idCategoriaOrdenar).orElseThrow(CategoryNotFoundException::new);
           platoEntityPage = platoRepository.findAllByIdRestauranteAndIdCategoriaAndActivo(restaurante,categoria,true,pageable);
        }
        return platoEntityMapper.toPlatoPage(platoEntityPage);
    }

    @Override
    public Boolean existPlatoByIdAndIdRestaurante(Long idPlato, Restaurante idRestaurante) {
        return platoRepository.existsByIdAndIdRestaurante(idPlato,restauranteEntityMapper.toEntity(idRestaurante));
    }

    @Override
    public Plato findDishById(Long idPlato) {
        return platoEntityMapper.toPlato(platoRepository.findById(idPlato).orElseThrow(DishNotFoundException::new));
    }

}
