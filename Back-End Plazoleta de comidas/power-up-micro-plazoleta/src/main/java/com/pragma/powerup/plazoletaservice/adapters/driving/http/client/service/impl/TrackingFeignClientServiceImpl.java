package com.pragma.powerup.plazoletaservice.adapters.driving.http.client.service.impl;

import com.pragma.powerup.plazoletaservice.adapters.driving.http.client.ITrackingFeignClient;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.client.service.ITrackingFeignClientService;
import com.pragma.powerup.plazoletaservice.domain.model.Tracking;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Map;
@Service
public class TrackingFeignClientServiceImpl implements ITrackingFeignClientService {
    @Autowired
    ITrackingFeignClient trackingFeignClient;
    @Override
    public ResponseEntity<Map<String, String>> trackingOrder(Tracking tracking) {
        return trackingFeignClient.trackingOrder(tracking);
    }
}
