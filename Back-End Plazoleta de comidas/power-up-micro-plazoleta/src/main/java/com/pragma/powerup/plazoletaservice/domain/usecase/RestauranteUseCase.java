package com.pragma.powerup.plazoletaservice.domain.usecase;

import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.exceptions.UnauthorizedUserException;
import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.exceptions.UserAlreadyExistsException;
import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.exceptions.UserNotFoundException;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.client.service.IUserClientService;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.request.EmployeeRequestDto;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.response.RankingEmployeesResDto;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.mapper.IEmpleadoRestauranteReqMapper;
import com.pragma.powerup.plazoletaservice.domain.api.IRestauranteServicePort;
import com.pragma.powerup.plazoletaservice.domain.exceptions.EmployeeNotSavedException;
import com.pragma.powerup.plazoletaservice.domain.model.EmpleadoRestaurante;
import com.pragma.powerup.plazoletaservice.domain.model.Restaurante;
import com.pragma.powerup.plazoletaservice.domain.spi.IRestaurantePersistencePort;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;


public class RestauranteUseCase implements IRestauranteServicePort {

    private final IRestaurantePersistencePort restaurantePersistencePort;
    private final IUserClientService userClientService;


    public RestauranteUseCase(IRestaurantePersistencePort restaurantePersistencePort, IUserClientService userClientService) {
        this.restaurantePersistencePort = restaurantePersistencePort;
        this.userClientService = userClientService;
    }

    @Override
    public void saveRestaurante(Restaurante restaurante) {
        if (Boolean.FALSE.equals(userClientService.existsOwner(restaurante.getIdPropietario()))){
            throw new UserNotFoundException();
        }
        restaurantePersistencePort.saveRestaurante(restaurante);
    }
    @Override
    public Page<Restaurante> getAllRestaurantes(int numeroPagina, int size) {
        return restaurantePersistencePort.getAllRestaurantesPage(numeroPagina,size);
    }

    @Override
    public void saveEmployee(EmployeeRequestDto employeeRequestDto, Long idUser) {
        Restaurante restaurante = new Restaurante(employeeRequestDto.getIdRestaurante(),null,null,null,null,null,null);
        if (!restaurantePersistencePort.existsByIdAndIdPropietario(employeeRequestDto.getIdRestaurante(), idUser)){
            throw new UnauthorizedUserException();
        }
        boolean empleadoExists=userClientService.existsEmployee(employeeRequestDto.getIdEmpleado());
        if (!empleadoExists){throw new UserNotFoundException();}
        if (restaurantePersistencePort.existsEmployeeByIdAndIdRestaurante(employeeRequestDto.getIdEmpleado(),restaurante)){throw new UserAlreadyExistsException();}
        EmpleadoRestaurante empleadoRestaurante = new EmpleadoRestaurante(employeeRequestDto.getIdEmpleado(),restaurante);
        restaurantePersistencePort.saveEmployee(empleadoRestaurante);

    }


}
