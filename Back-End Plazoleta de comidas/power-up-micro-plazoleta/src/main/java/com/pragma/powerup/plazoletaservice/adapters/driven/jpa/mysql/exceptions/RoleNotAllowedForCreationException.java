package com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.exceptions;

public class RoleNotAllowedForCreationException extends RuntimeException {
    public RoleNotAllowedForCreationException() {
        super();
    }
}
