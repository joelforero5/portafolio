package com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class RestauranteRequestDto {
    @Schema(title = "name", description = "name of a restaurant", example = "twenty one pilots - Ride")
    @NotBlank
    @Pattern(regexp = "^(?!\\d+$).+", message = "El nombre del restaurante no es válido, no se permiten nombres con solo números")
    private String nombre;
    @Schema(title = "adress", description = "address of a restaurant", example = "la victoria Mz 14 L8")
    @NotBlank
    private String direccion;
    @Schema(title = "idOwner", description = "restaurant owner id", example = "2")

    @NotNull
    private Long idPropietario;
    @Schema(title = "phone", description = "phone of a restaurant", example = "+573004055089")
    @NotBlank
    @Pattern(regexp = "^\\+?\\d{1,12}$", message = "El número de celular debe tener un formato valido")
    private String telefono;
    @Schema(title = "urlLogo", description = "restaurant logo url", example = "https://image-0.uhdpaper.com/wallpaper/eat-sleep-code-repeat-background-digital-art-4k-wallpaper-uhdpaper.com-249@0@g.jpg")
    @NotBlank
    private String urlLogo;
    @Schema(title = "nit", description = "nit of a restaurant (ONLY NUMBERS)", example = "10004413")
    @NotBlank
    @Pattern(regexp = "^[0-9]+$", message = "El numero de documento debe ser numérico")
    private String nit;
}
