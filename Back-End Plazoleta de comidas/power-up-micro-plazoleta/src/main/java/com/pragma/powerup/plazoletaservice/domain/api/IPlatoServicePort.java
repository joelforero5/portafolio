package com.pragma.powerup.plazoletaservice.domain.api;

import com.pragma.powerup.plazoletaservice.domain.model.Plato;
import org.springframework.data.domain.Page;

public interface IPlatoServicePort {
    void savePlato(Plato plato,Long idUser);
    void updatePlato(Plato plato,Long idUser);
    void updatePlatoActivo(Plato plato,Long idUser);
    Page<Plato> getAllPlatos(int numeroPagina, int size, Long idCategoriaOrdenar, Long idRestaurante,String campoOrdenar,String orden);
}
