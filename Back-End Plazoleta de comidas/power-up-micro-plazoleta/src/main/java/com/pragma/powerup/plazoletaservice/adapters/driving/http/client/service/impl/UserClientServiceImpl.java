package com.pragma.powerup.plazoletaservice.adapters.driving.http.client.service.impl;

import com.pragma.powerup.plazoletaservice.adapters.driving.http.client.UserClient;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.client.service.IUserClientService;

import org.springframework.stereotype.Service;

@Service
public class UserClientServiceImpl implements IUserClientService {
    private final UserClient userClient;

    public UserClientServiceImpl(UserClient userClient) {
        this.userClient = userClient;
    }

    @Override
    public Boolean existsOwner(Long id) {
        return userClient.getOwner(id).getBody();
    }

    @Override
    public Boolean existsClient(Long id) {
        return userClient.getClient(id).getBody();
    }

    @Override
    public Boolean existsEmployee(Long id) {
        return userClient.getEmployee(id).getBody();
    }

}
