package com.pragma.powerup.plazoletaservice.adapters.driving.http.client.service.impl;

import com.pragma.powerup.plazoletaservice.adapters.driving.http.client.IMessengerClient;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.client.service.IMessengerClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MessengerClientServiceImpl implements IMessengerClientService {
    @Autowired
    IMessengerClient messengerClient;
    @Override
    public void sendMessage(String statusOrder, String phoneNumber) {
        messengerClient.sendMessage(statusOrder,phoneNumber);
    }
}
