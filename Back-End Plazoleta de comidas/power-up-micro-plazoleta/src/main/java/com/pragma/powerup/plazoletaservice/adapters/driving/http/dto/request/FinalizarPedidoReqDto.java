package com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.request;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Positive;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class FinalizarPedidoReqDto {
    @NotNull(message = "The idOrder cannot be empty")
    @Positive(message = "The idOrder must be positive")
    private Long idPedido;

    @NotNull(message = "The pin of the order cannot be empty")
    @Pattern(regexp = "^([a-zA-Z0-9]){6}$", message = "The pin of the order must be 4 digits")
    private String pinPedido;
}
