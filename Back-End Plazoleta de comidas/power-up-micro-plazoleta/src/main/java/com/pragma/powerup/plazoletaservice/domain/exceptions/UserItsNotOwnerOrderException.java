package com.pragma.powerup.plazoletaservice.domain.exceptions;

public class UserItsNotOwnerOrderException extends RuntimeException{
    public UserItsNotOwnerOrderException(){super();}
}
