package com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.exceptions;

public class UserNotFoundException extends RuntimeException {
    public UserNotFoundException() {
        super();
    }
}
