package com.pragma.powerup.plazoletaservice.adapters.driving.http.handlers.impl;

import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.request.PlatoEnableDisableRequestDto;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.request.PlatoRequestDto;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.request.PlatoUpdateReqDto;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.response.PlatoResponseDto;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.handlers.IPlatoHandler;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.mapper.IPlatoRequestMapper;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.mapper.IPlatoResponseMapper;
import com.pragma.powerup.plazoletaservice.configuration.security.jwt.JwtService;
import com.pragma.powerup.plazoletaservice.domain.api.IPlatoServicePort;
import com.pragma.powerup.plazoletaservice.domain.model.Plato;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PlatoHandlerImpl implements IPlatoHandler {
    private final IPlatoServicePort platoServicePort;
    private final IPlatoRequestMapper platoRequestMapper;
    private final IPlatoResponseMapper platoResponseMapper;
    private final JwtService jwtService;
    private final HttpServletRequest request;
    @Override
    public void savePlato(PlatoRequestDto platoRequestDto) {
        Plato plato = platoRequestMapper.toPlato(platoRequestDto);
        platoServicePort.savePlato(plato,
                jwtService.getIdToken(request.getHeader("Authorization")));
    }

    @Override
    public void updatePlato(PlatoUpdateReqDto platoUpdateReqDto) {
        platoServicePort.updatePlato(platoRequestMapper.toPlatoUpdt(platoUpdateReqDto),
                jwtService.getIdToken(request.getHeader("Authorization")));

    }

    @Override
    public void updatePlatoActivo(PlatoEnableDisableRequestDto platoEnableDisableRequestDto) {
        platoServicePort.updatePlatoActivo(platoRequestMapper.toPlatoUpdt(platoEnableDisableRequestDto),
                jwtService.getIdToken(request.getHeader("Authorization")));

    }

    @Override
    public Page<PlatoResponseDto> getAllPlatos(int numeroPagina, int size, Long idCategoriaOrdenar, Long idRestaurante,String campoOrdenar,String orden) {
        return platoResponseMapper.toDtoPage(platoServicePort.getAllPlatos(
                numeroPagina,size,idCategoriaOrdenar,idRestaurante,campoOrdenar,orden));
    }
}
