package com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.response;



import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Date;
@AllArgsConstructor
@Getter
public class PedidoTiempoResDto {
    private Long id;
    private Long idCliente;
    private Date fecha;
    private Long idChef;
    private Double tiempoCompletado;


}
