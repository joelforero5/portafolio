package com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.exceptions.enums;

import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.exceptions.NotFoundException;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum NotFoundError {
    DISH("Dish not found"),
    USER("User not found");
    private final String description;
    public NotFoundException build(){
        return new NotFoundException(this);
    }
}
