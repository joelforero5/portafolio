package com.pragma.powerup.plazoletaservice.adapters.driving.http.handlers;

import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.request.FinalizarPedidoReqDto;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.request.PedidoReqDto;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.response.PedidoResDto;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.response.PedidoTiempoResDto;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.response.RankingEmployeesResDto;
import org.springframework.data.domain.Page;

public interface IPedidoHandler {
    void savePedido(PedidoReqDto pedidoReqDto);

    Page<PedidoResDto> getPedidos(int numeroPagina, int size, Long idEstado, Long idRestaurante, String campoOrdenar, String orden);

    void asignarPedido(Long idPedido);

    void pedidoListo(Long idPedido);

    void finalizarPedido(FinalizarPedidoReqDto finalizarPedidoReqDto);

    void cancelarPedido(Long idPedido);

    Page<PedidoTiempoResDto> getTiempoPedidos(int numeroPagina, int size, Long idRestaurante, String campoOrdenar, String orden);
    Page<RankingEmployeesResDto> getRankingEmployees(int numeroPagina, int size, Long idRestaurante);
}
