package com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.repositories;

import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.entity.EmpleadoRestauranteEntity;
import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.entity.RestauranteEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface IEmpleadoRestauranteRepository extends JpaRepository<EmpleadoRestauranteEntity,Long> {
    boolean existsByIdEmpleadoAndIdRestaurante(Long idEmpleado, RestauranteEntity idRestaurante);


}
