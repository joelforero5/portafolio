package com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class RankingEmployeesResDto {
    private Long idEmpleado;
    private Double tiempoPromedio;
}
