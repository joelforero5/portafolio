package com.pragma.powerup.plazoletaservice.adapters.driving.http.mapper;

import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.response.RestauranteResponseDto;
import com.pragma.powerup.plazoletaservice.domain.model.Restaurante;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface IRestauranteResponseMappper {

    RestauranteResponseDto toDto(Restaurante restaurante);

    List<RestauranteResponseDto> toDtoList(List<Restaurante> restaurantes);

    default Page<RestauranteResponseDto> toDtoPage(Page<Restaurante> page) {
        List<RestauranteResponseDto> responseDtos = toDtoList(page.getContent());
        return new PageImpl<>(responseDtos, page.getPageable(), page.getTotalElements());
    }
}
