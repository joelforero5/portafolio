package com.pragma.powerup.plazoletaservice.domain.api;

import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.request.EmployeeRequestDto;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.response.RankingEmployeesResDto;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.response.RestauranteResponseDto;
import com.pragma.powerup.plazoletaservice.domain.model.EmpleadoRestaurante;
import com.pragma.powerup.plazoletaservice.domain.model.Restaurante;
import org.springframework.data.domain.Page;

import java.util.List;

public interface IRestauranteServicePort {
    void saveRestaurante(Restaurante restaurante);
    Page<Restaurante> getAllRestaurantes(int numeroPagina, int tamaño);

    void saveEmployee(EmployeeRequestDto employeeRequestDto, Long idUser);


}
