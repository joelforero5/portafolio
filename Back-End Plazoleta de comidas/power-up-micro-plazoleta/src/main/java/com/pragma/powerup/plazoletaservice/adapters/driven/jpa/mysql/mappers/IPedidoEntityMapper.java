package com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.mappers;

import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.entity.PedidoEntity;
import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.entity.PlatoEntity;
import com.pragma.powerup.plazoletaservice.domain.model.Pedido;
import com.pragma.powerup.plazoletaservice.domain.model.Plato;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface IPedidoEntityMapper {
    IPlatoEntityMapper INSTANCE= Mappers.getMapper(IPlatoEntityMapper.class);
    PedidoEntity toEntity(Pedido pedido);
    Pedido toPedido (PedidoEntity pedidoEntity);

    List<Pedido> toPedidoList(List<PedidoEntity> pedidoEntityList);

    default Page<Pedido> toPedidoPage(Page<PedidoEntity> page) {
        List<Pedido> pedidoList = toPedidoList(page.getContent());
        return new PageImpl<>(pedidoList, page.getPageable(), page.getTotalElements());
    }
}
