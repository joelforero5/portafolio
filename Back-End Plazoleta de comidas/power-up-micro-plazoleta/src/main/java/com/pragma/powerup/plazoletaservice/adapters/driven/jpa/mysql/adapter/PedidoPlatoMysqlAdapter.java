package com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.adapter;

import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.entity.PedidoPlatoEntity;
import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.mappers.IPedidoPlatoEntityMapper;
import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.repositories.IPedidoPlatoRepository;
import com.pragma.powerup.plazoletaservice.domain.model.PedidoPlato;
import com.pragma.powerup.plazoletaservice.domain.spi.IPedidoPlatoPersistencePort;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
@Transactional
public class PedidoPlatoMysqlAdapter implements IPedidoPlatoPersistencePort {
    private final IPedidoPlatoRepository pedidoPlatoRepository;
    private final IPedidoPlatoEntityMapper pedidoPlatoEntityMapper;

    @Override
    public void saveOrderDishes(List<PedidoPlato> orderDishEntities) {
        List<PedidoPlatoEntity> platos = pedidoPlatoEntityMapper.toEntityList(orderDishEntities);
        pedidoPlatoRepository.saveAll(platos);
    }
}
