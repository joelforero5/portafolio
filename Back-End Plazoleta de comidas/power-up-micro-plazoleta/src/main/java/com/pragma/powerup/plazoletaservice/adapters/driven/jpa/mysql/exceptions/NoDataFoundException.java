package com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.exceptions;

public class NoDataFoundException extends RuntimeException{
    public NoDataFoundException() {
        super();
    }
}
