package com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.entity;


import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "plato")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class PlatoEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nombre;
    @ManyToOne
    @JoinColumn(name="categoria_id")
    private CategoriaEntity idCategoria;
    private String descripcion;
    private Long precio;
    @ManyToOne
    @JoinColumn(name="restaurante_id")
    private RestauranteEntity idRestaurante;
    private String urlImagen;
    private boolean activo;
}
