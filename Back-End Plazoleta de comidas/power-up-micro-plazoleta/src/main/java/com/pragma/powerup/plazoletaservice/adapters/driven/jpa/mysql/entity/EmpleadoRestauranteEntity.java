package com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "empleados_restaurantes")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class EmpleadoRestauranteEntity {
    @Id
    @Column(name = "id_empleado",unique = true)
    private Long idEmpleado;
    @ManyToOne
    @JoinColumn(name="restaurante_id")
    private RestauranteEntity idRestaurante;
}
