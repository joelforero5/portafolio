package com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Entity
@Table(name = "pedidos")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class PedidoEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "id_cliente")
    private Long idCliente;

    private Date fecha;

    @ManyToOne
    @JoinColumn(name ="id_status")
    private PedidoStatusEntity idStatus;

    @ManyToOne
    @JoinColumn(name = "id_empleado")
    private EmpleadoRestauranteEntity idChef;

    @ManyToOne
    @JoinColumn(name ="id_restaurante")
    private RestauranteEntity idRestaurante;
    @JoinColumn(name ="tiempo_completado")
    private Double tiempoCompletado;
    @JoinColumn(name ="pin_pedido")
    private String pinPedido;


}
