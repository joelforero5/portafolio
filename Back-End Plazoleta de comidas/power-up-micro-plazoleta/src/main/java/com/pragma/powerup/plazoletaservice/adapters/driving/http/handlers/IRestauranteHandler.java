package com.pragma.powerup.plazoletaservice.adapters.driving.http.handlers;

import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.request.EmployeeRequestDto;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.request.RestauranteRequestDto;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.response.RankingEmployeesResDto;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.response.RestauranteResponseDto;
import org.springframework.data.domain.Page;

import java.util.List;

public interface IRestauranteHandler {
    void saveRestaurante(RestauranteRequestDto restauranteRequestDto);
    Page<RestauranteResponseDto> getAllRestaurantes(int numeroPagina, int tamaño);

    void saveEmployee(EmployeeRequestDto userRequestDto);


}
