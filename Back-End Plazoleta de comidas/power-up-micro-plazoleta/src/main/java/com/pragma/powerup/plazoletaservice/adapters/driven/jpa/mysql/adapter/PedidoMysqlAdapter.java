package com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.adapter;


import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.entity.EmpleadoRestauranteEntity;
import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.entity.PedidoEntity;
import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.entity.PedidoStatusEntity;
import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.entity.RestauranteEntity;
import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.exceptions.*;

import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.mappers.IPedidoEntityMapper;


import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.repositories.IEmpleadoRestauranteRepository;
import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.repositories.IPedidoRepository;
import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.repositories.IRestauranteRepository;

import com.pragma.powerup.plazoletaservice.domain.model.Pedido;


import com.pragma.powerup.plazoletaservice.domain.spi.IPedidoPersistencePort;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.annotation.Transactional;

import static com.pragma.powerup.plazoletaservice.configuration.Constants.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Transactional
public class PedidoMysqlAdapter implements IPedidoPersistencePort {
    private final IPedidoRepository pedidoRepository;
    private final IPedidoEntityMapper pedidoEntityMapper;
    private final IRestauranteRepository restauranteRepository;
    private final IEmpleadoRestauranteRepository empleadoRestauranteRepository;

    @Override
    public Long savePedido(Pedido order) {
        RestauranteEntity restaurante = restauranteRepository.findById(order.getIdRestaurante().getId()).orElseThrow(RestauranteNotFoundException::new);
        PedidoEntity pedido = pedidoEntityMapper.toEntity(order);
        pedido.setIdRestaurante(restaurante);
        pedido = pedidoRepository.save(pedido);
        return pedido.getId();
    }

    @Override
    public void deleteOrderById(Long idOrder) {
        pedidoRepository.deleteById(idOrder);
    }

    @Override
    public Page<Pedido> getPedidos(int numeroPagina, int size, Long idEstado, Long idRestaurante, String campoOrdenar, String orden) {
        Sort sort = Sort.by(orden.equalsIgnoreCase("desc") ? Sort.Direction.DESC : Sort.Direction.ASC, campoOrdenar);
        Pageable pageable = PageRequest.of(numeroPagina, size, sort);
        PedidoStatusEntity estado = new PedidoStatusEntity();
        estado.setId(idEstado);
        Page<PedidoEntity> pedidoEntityPage = pedidoRepository.findByIdStatus(estado, pageable);
        return pedidoEntityMapper.toPedidoPage(pedidoEntityPage);
    }

    public void userCanCreateNewOrder(Long idUser) {
        List<PedidoStatusEntity> pedidoStatusEntityList = new ArrayList<>();
        pedidoStatusEntityList.add(new PedidoStatusEntity(STATUS_ORDER_IN_PROGRESS_ID));
        pedidoStatusEntityList.add(new PedidoStatusEntity(STATUS_ORDER_IN_PENDING_ID));
        pedidoStatusEntityList.add(new PedidoStatusEntity(STATUS_ORDER_IN_PREPARATION_ID));
        pedidoStatusEntityList.add(new PedidoStatusEntity(STATUS_ORDER_IN_READY_ID));
        Optional<PedidoEntity> orderEntity = pedidoRepository.findFirstByIdClienteAndIdStatusIn(idUser, pedidoStatusEntityList);
        if (orderEntity.isPresent()) {
            throw new UserWithOrderInProgressException();
        }
    }

    @Override
    public void asignarPedido(Long idPedido, Long idUser) {
        PedidoEntity pedido = pedidoRepository.findById(idPedido).orElseThrow(NoDataFoundException::new);
        boolean existsEmployee = empleadoRestauranteRepository.existsByIdEmpleadoAndIdRestaurante(idUser, pedido.getIdRestaurante());
        if (!existsEmployee) {
            throw new UnauthorizedUserException();
        }
        if (pedido.getId().equals(STATUS_ORDER_IN_PREPARATION_ID)) {
            throw new OrderIsAlreadyTakenException();
        }
        if (pedido.getIdChef() != null) {
            throw new OrderCannotBeTakenException();
        }
        pedido.setIdStatus(new PedidoStatusEntity(STATUS_ORDER_IN_PREPARATION_ID));
        EmpleadoRestauranteEntity empleadoRestaurante = new EmpleadoRestauranteEntity();
        empleadoRestaurante.setIdEmpleado(idUser);
        pedido.setIdChef(empleadoRestaurante);
        pedidoRepository.save(pedido);
    }

    @Override
    public PedidoEntity findPedidoById(Long idPedido) {
        return pedidoRepository.findById(idPedido).orElseThrow(NoDataFoundException::new);
    }

    @Override
    public void pedidoListo(PedidoEntity pedido) {
        pedido.setIdStatus(new PedidoStatusEntity(STATUS_ORDER_IN_READY_ID));
        pedidoRepository.save(pedido);
    }

    @Override
    public void finalizarPedido(PedidoEntity pedido) {
        pedido.setIdStatus(new PedidoStatusEntity(STATUS_ORDER_FINISHED));
        pedidoRepository.save(pedido);
    }

    @Override
    public void cancelarPedido(PedidoEntity pedido) {
        pedido.setIdStatus(new PedidoStatusEntity(STATUS_ORDER_CANCELLED_ID));
        pedidoRepository.save(pedido);
    }

    @Override
    public PedidoEntity toPedidoEntity(Pedido pedido) {
        return pedidoEntityMapper.toEntity(pedido);
    }

    @Override
    public Page<Pedido> getTiempoPedidos(int numeroPagina, int size, Long idRestaurante, String campoOrdenar, String orden) {
        Sort sort = Sort.by(orden.equalsIgnoreCase("desc") ? Sort.Direction.DESC : Sort.Direction.ASC, campoOrdenar);
        Pageable pageable = PageRequest.of(numeroPagina, size, sort);
        RestauranteEntity restaurante = new RestauranteEntity();
        restaurante.setId(idRestaurante);
        Page<PedidoEntity> pedidoEntityPage = pedidoRepository.findByIdRestauranteAndTiempoCompletadoIsNotNull(restaurante, pageable);
        return pedidoEntityMapper.toPedidoPage(pedidoEntityPage);
    }
    @Override
    public Page<Object[]> obtenerRankingEmployeesPorTiempoPromedio(int numeroPagina, int size, Long idRestaurante) {
        Sort sort = Sort.by(Sort.Direction.ASC,"tiempoPromedio");
        Pageable pageable = PageRequest.of(numeroPagina, size,sort);
        return pedidoRepository.obtenerRankingEmployeesPorTiempoPromedio(idRestaurante, pageable);
    }
}
