package com.pragma.powerup.plazoletaservice.adapters.driving.http.client.service;

public interface IMessengerClientService {
    void sendMessage(String statusOrder, String phoneNumber);
}
