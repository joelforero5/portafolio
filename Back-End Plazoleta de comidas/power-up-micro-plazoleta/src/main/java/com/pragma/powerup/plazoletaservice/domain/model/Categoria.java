package com.pragma.powerup.plazoletaservice.domain.model;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Categoria {
    private Long id;
    private String nombre;
    private String descripcion;

}
