package com.pragma.powerup.plazoletaservice.domain.spi;


import com.pragma.powerup.plazoletaservice.domain.model.PedidoPlato;

import java.util.ArrayList;
import java.util.List;

public interface IPedidoPlatoPersistencePort {
    void saveOrderDishes(List<PedidoPlato> orderDishEntities);
}
