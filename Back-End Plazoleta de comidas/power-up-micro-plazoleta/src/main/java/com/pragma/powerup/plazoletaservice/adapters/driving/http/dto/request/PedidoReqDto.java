package com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.request;

import com.pragma.powerup.plazoletaservice.adapters.driving.http.assets.PlatoAsset;
import com.pragma.powerup.plazoletaservice.domain.model.PedidoPlato;

import jakarta.validation.constraints.NotNull;
import lombok.Data;


import java.util.ArrayList;
import java.util.List;
@Data
public class PedidoReqDto {
    @NotNull(message = "The idRestaurant cannot be empty")
    private Long idRestaurante;

    @NotNull(message = "The dishes cannot be empty")
    ArrayList<PlatoAsset> platos;

}
