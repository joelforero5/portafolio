package com.pragma.powerup.plazoletaservice.adapters.driving.http.assets;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class PlatoAsset {
    private Long idPlato;
    private Long cantidad;
}
