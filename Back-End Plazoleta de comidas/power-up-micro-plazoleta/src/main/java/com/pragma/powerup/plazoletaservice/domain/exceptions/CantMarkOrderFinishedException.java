package com.pragma.powerup.plazoletaservice.domain.exceptions;

public class CantMarkOrderFinishedException extends RuntimeException{
    public CantMarkOrderFinishedException(){super();}
}
