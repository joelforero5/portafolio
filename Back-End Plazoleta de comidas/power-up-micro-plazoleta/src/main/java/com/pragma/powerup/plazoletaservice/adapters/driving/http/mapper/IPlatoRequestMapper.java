package com.pragma.powerup.plazoletaservice.adapters.driving.http.mapper;

import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.request.PlatoEnableDisableRequestDto;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.request.PlatoRequestDto;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.request.PlatoUpdateReqDto;
import com.pragma.powerup.plazoletaservice.domain.model.Plato;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface IPlatoRequestMapper {
    @Mapping(target = "idRestaurante.id",source = "idRestaurante")
    @Mapping(target = "idCategoria.id",source = "idCategoria")
    Plato toPlato (PlatoRequestDto platoRequestDto);
    Plato toPlatoUpdt(PlatoUpdateReqDto platoUpdateReqDto);
    Plato toPlatoUpdt(PlatoEnableDisableRequestDto platoEnableDisableRequestDto);

}
