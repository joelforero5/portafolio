package com.pragma.powerup.plazoletaservice.domain.exceptions;

public class UserCantCancelOrderException extends RuntimeException{
    public UserCantCancelOrderException(){super();}
}
