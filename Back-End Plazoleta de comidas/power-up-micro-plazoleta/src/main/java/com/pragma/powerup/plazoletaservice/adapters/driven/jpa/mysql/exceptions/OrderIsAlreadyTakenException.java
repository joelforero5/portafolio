package com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.exceptions;

import org.aspectj.weaver.ast.Or;

public class OrderIsAlreadyTakenException extends RuntimeException{
    public OrderIsAlreadyTakenException(){
        super();
    }
}
