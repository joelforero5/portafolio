package com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.exceptions;

public class PersonNotFoundException extends RuntimeException {
    public PersonNotFoundException() {
        super();
    }
}
