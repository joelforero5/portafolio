package com.pragma.powerup.plazoletaservice.domain.spi;

import com.pragma.powerup.plazoletaservice.domain.model.Plato;
import com.pragma.powerup.plazoletaservice.domain.model.Restaurante;
import org.springframework.data.domain.Page;

import java.util.Optional;

public interface IPlatoPersistencePort {
    void savePlato (Plato plato,Long idUser);
    void updatePlato (Plato plato,Long idUser);
    void updatePlatoActivo (Plato plato,Long idUser);
    Page<Plato>getAllPlatos(int numeroPagina, int size, Long idCategoriaOrdenar, Long idRestaurante,String campoOrdenar,String orden);

    Boolean existPlatoByIdAndIdRestaurante(Long idPlato, Restaurante idRestaurante);

    Plato findDishById(Long idPlato);
}
