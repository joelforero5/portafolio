package com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.exceptions;

public class UnauthorizedUserException extends RuntimeException{
    public UnauthorizedUserException() {
        super();
    }
}
