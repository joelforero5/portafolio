package com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.exceptions;

import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.exceptions.enums.NotFoundError;

public class NotFoundException extends RuntimeException{
    private final NotFoundError error;
    public NotFoundException(NotFoundError error){
        super(error.getDescription());
        this.error = error;
    }

}
