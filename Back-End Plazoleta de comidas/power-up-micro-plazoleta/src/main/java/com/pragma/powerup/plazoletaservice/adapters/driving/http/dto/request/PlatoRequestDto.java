package com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.request;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class PlatoRequestDto {
    @Schema(title = "nombre", description = "Name of a dish", example = "hamburger")
    @NotBlank
    private String nombre;
    @Schema(title = "precio", description = "Price of a dish", example = "50000.00")
    @NotNull
    @Positive
    private Long precio;
    @Schema(title = "descripcion", description = "Description of a dish", example = "-two portions of meat\n-cheese\n-bacon")
    @NotNull
    @NotBlank
    private String descripcion;
    @Schema(title = "urlImagen", description = "Url of the image of a dish", example = "https://image-0.uhdpaper.com/wallpaper/eat-sleep-code-repeat-background-digital-art-4k-wallpaper-uhdpaper.com-249@0@g.jpg")
    @NotNull
    private String urlImagen;
    @Schema(title = "idCategoria", description = "Category of a dish", example = "1")
    @NotNull
    private Long idCategoria;
    @Schema(title = "idRestaurante", description = "Restaurant of a dish", example = "1")
    @NotNull
    private Long idRestaurante;


}
