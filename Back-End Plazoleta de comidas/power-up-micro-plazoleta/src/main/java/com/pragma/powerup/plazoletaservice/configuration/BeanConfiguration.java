package com.pragma.powerup.plazoletaservice.configuration;

import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.adapter.PedidoMysqlAdapter;
import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.adapter.PedidoPlatoMysqlAdapter;
import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.adapter.PlatoMysqlAdapter;
import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.adapter.RestauranteMysqlAdapter;
import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.mappers.*;
import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.repositories.*;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.client.ITrackingFeignClient;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.client.service.IMessengerClientService;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.client.service.ITrackingFeignClientService;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.client.service.IUserClientService;
import com.pragma.powerup.plazoletaservice.domain.api.IPedidoPlatoServicePort;
import com.pragma.powerup.plazoletaservice.domain.api.IPedidoServicePort;
import com.pragma.powerup.plazoletaservice.domain.api.IPlatoServicePort;
import com.pragma.powerup.plazoletaservice.domain.api.IRestauranteServicePort;
import com.pragma.powerup.plazoletaservice.domain.spi.IPedidoPersistencePort;
import com.pragma.powerup.plazoletaservice.domain.spi.IPedidoPlatoPersistencePort;
import com.pragma.powerup.plazoletaservice.domain.spi.IPlatoPersistencePort;
import com.pragma.powerup.plazoletaservice.domain.spi.IRestaurantePersistencePort;
import com.pragma.powerup.plazoletaservice.domain.usecase.PedidoPlatoUseCase;
import com.pragma.powerup.plazoletaservice.domain.usecase.PedidoUseCase;
import com.pragma.powerup.plazoletaservice.domain.usecase.PlatoUseCase;
import com.pragma.powerup.plazoletaservice.domain.usecase.RestauranteUseCase;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class BeanConfiguration {
    private final IRestauranteRepository restauranteRepository;
    private final IEmpleadoRestauranteRepository empleadoRestauranteRepository;
    private final IRestauranteEntityMapper restauranteEntityMapper;
    private final IEmpleadoRestauranteEntityMapper empleadoRestauranteEntityMapper;

    private final IUserClientService userClientService;
    private final IMessengerClientService messengerClientService;
    private final ITrackingFeignClientService trackingFeignClientService;

    private final IPlatoRepository platoRepository;
    private final IPlatoEntityMapper platoEntityMapper;

    private final ICategoriaRepository categoriaRepository;

    private final IPedidoRepository pedidoRepository;
    private final IPedidoEntityMapper pedidoEntityMapper;

    private final IPedidoPlatoRepository pedidoPlatoRepository;
    private final IPedidoPlatoEntityMapper pedidoPlatoEntityMapper;


    @Bean
    public IRestaurantePersistencePort restaurantePersistencePort() {
        return new RestauranteMysqlAdapter(restauranteRepository, restauranteEntityMapper,empleadoRestauranteRepository,empleadoRestauranteEntityMapper);
    }
    @Bean
    public IRestauranteServicePort restauranteServicePort() {
        return new RestauranteUseCase(restaurantePersistencePort(), userClientService);
    }
    @Bean
    public IPlatoPersistencePort platoPersistencePort(){
        return new PlatoMysqlAdapter(platoRepository,platoEntityMapper,categoriaRepository,restauranteRepository,restauranteEntityMapper);
    }
    @Bean
    public IPlatoServicePort platoServicePort(){
        return new PlatoUseCase(platoPersistencePort());
    }

    @Bean
    public IPedidoPlatoPersistencePort pedidoPlatoPersistencePort(){
        return new PedidoPlatoMysqlAdapter(pedidoPlatoRepository,pedidoPlatoEntityMapper);
    }
    @Bean
    public IPedidoPlatoServicePort pedidoPlatoServicePort(){
        return new PedidoPlatoUseCase(pedidoPlatoPersistencePort());
    }


    @Bean
    public IPedidoPersistencePort pedidoPersistencePort(){
        return new PedidoMysqlAdapter(pedidoRepository,pedidoEntityMapper,restauranteRepository,empleadoRestauranteRepository);
    }
    @Bean
    public IPedidoServicePort pedidoServicePort(){
        return new PedidoUseCase(pedidoPersistencePort(),userClientService, trackingFeignClientService, platoPersistencePort(),
                pedidoPlatoPersistencePort(), restaurantePersistencePort(),messengerClientService);
    }



}
