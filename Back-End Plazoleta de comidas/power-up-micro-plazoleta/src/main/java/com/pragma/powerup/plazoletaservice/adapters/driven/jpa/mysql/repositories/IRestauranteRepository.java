package com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.repositories;

import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.entity.EmpleadoRestauranteEntity;
import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.entity.RestauranteEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface IRestauranteRepository extends JpaRepository<RestauranteEntity,Long> {
    Optional<RestauranteEntity> findByIdAndIdPropietario(Long id, Long idOwner);
    boolean existsByIdAndIdPropietario(Long id,Long idPropietario);
    Page<RestauranteEntity> findAll(Pageable pageable);
}
