package com.pragma.powerup.plazoletaservice.domain.exceptions;

public class EmployeeNotSavedException extends RuntimeException {
    public EmployeeNotSavedException(){super();}
}
