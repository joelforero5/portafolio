package com.pragma.powerup.plazoletaservice.domain.usecase;

import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.entity.PedidoEntity;
import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.exceptions.UnauthorizedUserException;
import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.exceptions.UserNotFoundException;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.assets.PlatoAsset;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.client.service.IMessengerClientService;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.client.service.ITrackingFeignClientService;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.client.service.IUserClientService;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.request.FinalizarPedidoReqDto;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.request.PedidoReqDto;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.response.RankingEmployeesResDto;
import com.pragma.powerup.plazoletaservice.domain.api.IPedidoServicePort;
import com.pragma.powerup.plazoletaservice.domain.exceptions.*;
import com.pragma.powerup.plazoletaservice.domain.model.*;
import com.pragma.powerup.plazoletaservice.domain.spi.IPedidoPersistencePort;
import com.pragma.powerup.plazoletaservice.domain.spi.IPedidoPlatoPersistencePort;
import com.pragma.powerup.plazoletaservice.domain.spi.IPlatoPersistencePort;
import com.pragma.powerup.plazoletaservice.domain.spi.IRestaurantePersistencePort;
import com.pragma.powerup.plazoletaservice.domain.usecase.utils.CodeGenerator;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.Temporal;
import java.util.*;
import java.util.stream.Collectors;

import static com.pragma.powerup.plazoletaservice.configuration.Constants.*;

public class PedidoUseCase implements IPedidoServicePort {
    private final IPedidoPersistencePort pedidoPersistencePort;
    private final IUserClientService userClientService;
    private final ITrackingFeignClientService trackingFeignClientService;
    private final IPlatoPersistencePort platoPersistencePort;
    private final IPedidoPlatoPersistencePort pedidoPlatoPersistencePort;
    private final IRestaurantePersistencePort restaurantePersistencePort;
    private final IMessengerClientService messengerClientService;

    public PedidoUseCase(IPedidoPersistencePort pedidoPersistencePort, IUserClientService userClientService, ITrackingFeignClientService trackingFeignClientService, IPlatoPersistencePort platoPersistencePort, IPedidoPlatoPersistencePort pedidoPlatoPersistencePort, IRestaurantePersistencePort restaurantePersistencePort, IMessengerClientService messengerClientService) {
        this.pedidoPersistencePort = pedidoPersistencePort;
        this.userClientService = userClientService;
        this.trackingFeignClientService = trackingFeignClientService;
        this.platoPersistencePort = platoPersistencePort;
        this.pedidoPlatoPersistencePort = pedidoPlatoPersistencePort;
        this.restaurantePersistencePort = restaurantePersistencePort;
        this.messengerClientService = messengerClientService;
    }

    @Override
    public void savePedido(PedidoReqDto pedidoReqDto,Long idUser) {
        if (Boolean.FALSE.equals(userClientService.existsClient(idUser))){
            throw new UserNotFoundException();
        }
        pedidoPersistencePort.userCanCreateNewOrder(idUser);
        Restaurante restaurante = new Restaurante(pedidoReqDto.getIdRestaurante(),null,null,null,null,null,null);
        Pedido pedido = new Pedido();
        pedido.setIdCliente(idUser);
        pedido.setFecha(new Date());
        pedido.setIdStatus(new PedidoStatus(STATUS_ORDER_IN_PENDING_ID,null,null));
        pedido.setIdRestaurante(restaurante);
        Long idPedido = pedidoPersistencePort.savePedido(pedido);
        pedido.setId(idPedido);
        ArrayList<PedidoPlato> platos = validateDishesToSave(pedidoReqDto.getPlatos(),restaurante,pedido);
        pedidoPlatoPersistencePort.saveOrderDishes(platos);
        sendNotificationToUser("Order #"+pedido.getId()+" created successfully", "+573188227631");
        createLoggOrder(pedidoPersistencePort.toPedidoEntity(pedido), "",STATUS_ORDER_PENDING);

    }

    private void createLoggOrder(PedidoEntity pedido, String previousStatus, String currentStatus) {
        Tracking tracking = initializeTracking(pedido, previousStatus, currentStatus);
        trackingFeignClientService.trackingOrder(tracking);
    }
    private Tracking initializeTracking(PedidoEntity pedido, String previousStatus, String currentStatus){
        Tracking tracking = new Tracking();
        tracking.setIdOrder(pedido.getId());
        tracking.setIdEmployee(pedido.getIdChef().getIdEmpleado());
        tracking.setIdCustomer(pedido.getIdCliente());
        tracking.setIdRestaurant(pedido.getIdRestaurante().getId());
        tracking.setPreviousStatus(previousStatus);
        tracking.setCurrentStatus(currentStatus);
        return tracking;
    }


    @Override
    public void asignarPedido(Long idPedido, Long idUser) {
        if (idPedido < 0) {
            throw new ParametersNegativesException();
        }
        pedidoPersistencePort.asignarPedido(idPedido,idUser);
        sendNotificationToUser("Order #"+idPedido+" is in preparation", "+573188227631");
        PedidoEntity pedido = pedidoPersistencePort.findPedidoById(idPedido);
        createLoggOrder(pedido, STATUS_ORDER_PENDING,STATUS_ORDER_IN_PREPARATION);

    }

    @Override
    public Page<Pedido> getPedidos(int numeroPagina, int size, Long idEstado, Long idRestaurante, String campoOrdenar, String orden, Long authorization) {
        if (numeroPagina < 0 || size < 0 || idEstado < 0 ) {
            throw new ParametersNegativesException();
        }
        Restaurante restaurante = new Restaurante(idRestaurante,null,null,null,null,null,null);
        if (!restaurantePersistencePort.existsEmployeeByIdAndIdRestaurante(authorization,restaurante)){throw new UnauthorizedUserException();}
        return pedidoPersistencePort.getPedidos(numeroPagina,size,idEstado,idRestaurante,campoOrdenar,orden);
    }

    @Override
    public void pedidoListo(Long idPedido, Long authorization) {
        if (idPedido < 0) {
            throw new ParametersNegativesException();
        }
        PedidoEntity pedido = pedidoPersistencePort.findPedidoById(idPedido);
        if (!pedido.getIdStatus().getName().equals(STATUS_ORDER_IN_PREPARATION)){
            throw new CantMarkOrderReadyException();
        }
        if (!pedido.getIdChef().getIdEmpleado().equals(authorization)){ throw new UserCantMarkOrderReadyException();}
        pedido.setPinPedido((generarPin()));
        pedidoPersistencePort.pedidoListo(pedido);
        sendNotificationToUser("Order #" + idPedido + " is ready to claim with this code: "+ pedido.getPinPedido(), "+573188227631");
        createLoggOrder(pedido, STATUS_ORDER_IN_PREPARATION,STATUS_ORDER_READY);

    }

    @Override
    public void finalizarPedido(FinalizarPedidoReqDto finalizarPedidoReqDto, Long authorization) {
        if (finalizarPedidoReqDto.getIdPedido() < 0) {
            throw new ParametersNegativesException();
        }
        PedidoEntity pedido = pedidoPersistencePort.findPedidoById(finalizarPedidoReqDto.getIdPedido());
        if(pedido.getIdStatus().getName().equals(STATUS_ORDER_READY)) {
            if (!Objects.equals(pedido.getIdChef().getIdEmpleado(), authorization)) {
                throw new UserCantFinishedOrderException();
            }

            if(!pedido.getPinPedido().equals(finalizarPedidoReqDto.getPinPedido())){
                throw new PinWrongException();
            }
            pedido.setTiempoCompletado(calcCompletionTimeOfTheOrder(pedido.getFecha()));
            pedidoPersistencePort.finalizarPedido(pedido);
            sendNotificationToUser("Order #" + finalizarPedidoReqDto.getIdPedido() + " finished correctly", "+573188227631");
            createLoggOrder(pedido, STATUS_ORDER_READY,STATUS_ORDER_FINISHED_NAME);
        }else{
            throw new CantMarkOrderFinishedException();
        }
    }

    @Override
    public void cancelarPedido(Long idPedido, Long authorization) {
        if (idPedido < 0) {
            throw new ParametersNegativesException();
        }
        PedidoEntity pedido = pedidoPersistencePort.findPedidoById(idPedido);

        if(!Objects.equals(pedido.getIdCliente(), authorization)){
            throw new UserItsNotOwnerOrderException();
        }

        if(pedido.getIdStatus().getName().equals(STATUS_ORDER_PENDING)) {
            pedidoPersistencePort.cancelarPedido(pedido);
            sendNotificationToUser("Order #" + idPedido + " was cancelled correctly", "+573188227631");
        }else{
            throw new UserCantCancelOrderException();
        }
    }

    @Override
    public Page<Pedido> getTiempoPedidos(int numeroPagina, int size, Long idRestaurante, String campoOrdenar, String orden, Long authorization) {
        if (numeroPagina < 0 || size < 0 || idRestaurante < 0 ) {
            throw new ParametersNegativesException();
        }
        if (!restaurantePersistencePort.existsByIdAndIdPropietario(idRestaurante,authorization)){throw new UnauthorizedUserException();}

        return pedidoPersistencePort.getTiempoPedidos(numeroPagina,size,idRestaurante,campoOrdenar,orden);
    }
    @Override
    public Page<RankingEmployeesResDto> getRankingEmployees(int numeroPagina, int size, Long idRestaurante, Long authorization) {
        if (!restaurantePersistencePort.existsByIdAndIdPropietario(idRestaurante, authorization)) {
            throw new UnauthorizedUserException();
        }
        Page<Object[]> resultados = pedidoPersistencePort.obtenerRankingEmployeesPorTiempoPromedio(numeroPagina, size, idRestaurante);
        List<RankingEmployeesResDto> ranking = resultados.getContent().stream()
                .map(obj -> {
                    Long idEmpleado = (Long) obj[0];
                    Double tiempoPromedio = (Double) obj[1];
                    return new RankingEmployeesResDto(idEmpleado, tiempoPromedio);
                })
                .collect(Collectors.toList());
        return new PageImpl<>(ranking, resultados.getPageable(), resultados.getTotalElements());
    }

    private Double calcCompletionTimeOfTheOrder(Date fecha) {
        //Eficiencia de pedidos
        LocalDateTime dateNow = LocalDateTime.now();

        Temporal temporal = LocalDateTime.from(fecha.toInstant().atZone(ZoneId.systemDefault()));

        Duration duration = Duration.between(temporal, dateNow);
        long minutes = duration.toMinutes();

        return (double) minutes;
    }

    private void sendNotificationToUser(String statusOrder, String phoneNumber){
        messengerClientService.sendMessage(statusOrder, phoneNumber);
    }

    private String generarPin() {
        return CodeGenerator.generateCode();
    }

    private void deleteOrderById(Long idOrder){
        pedidoPersistencePort.deleteOrderById(idOrder);
    }

    protected ArrayList<PedidoPlato> validateDishesToSave(ArrayList<PlatoAsset> platosReq,Restaurante idRestaurante ,Pedido order){
        ArrayList<PedidoPlato> dishesToSave = new ArrayList<>();
        for(PlatoAsset dish : platosReq){
            Boolean dishExist = platoPersistencePort.existPlatoByIdAndIdRestaurante(dish.getIdPlato(),idRestaurante);
            if(Boolean.TRUE.equals(dishExist)) {
                Plato platoEncontrado = platoPersistencePort.findDishById(dish.getIdPlato());
                if (dish.getCantidad() <= 0) {
                    deleteOrderById(order.getId());
                    throw new QuantityDishInvalidException();
                }
                dishesToSave.add(new PedidoPlato(null,order,platoEncontrado,dish.getCantidad()));
            }else{
                deleteOrderById(order.getId());
                throw new SomeDishesAreNotFromRestaurantException();
            }
        }
        return dishesToSave;
    }
}
