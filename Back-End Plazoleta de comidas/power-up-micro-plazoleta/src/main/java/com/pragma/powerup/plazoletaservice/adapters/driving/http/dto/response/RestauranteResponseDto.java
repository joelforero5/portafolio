package com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
@AllArgsConstructor
@Getter
public class RestauranteResponseDto {
    private String nombre;
    private String urlLogo;

}
