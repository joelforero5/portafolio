package com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Date;
@AllArgsConstructor
@Getter
public class PedidoResDto {
    private Long id;
    private Long idCliente;
    private Date fecha;
    private String idStatus;
    private Long idChef;
    private String idRestaurante;
}
