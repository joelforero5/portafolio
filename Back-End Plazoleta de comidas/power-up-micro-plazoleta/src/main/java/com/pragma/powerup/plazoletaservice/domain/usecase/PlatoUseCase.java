package com.pragma.powerup.plazoletaservice.domain.usecase;

import com.pragma.powerup.plazoletaservice.domain.api.IPlatoServicePort;
import com.pragma.powerup.plazoletaservice.domain.model.Plato;
import com.pragma.powerup.plazoletaservice.domain.spi.IPlatoPersistencePort;
import org.springframework.data.domain.Page;

public class PlatoUseCase implements IPlatoServicePort {
    private final IPlatoPersistencePort platoPersistencePort;

    public PlatoUseCase(IPlatoPersistencePort platoPersistencePort) {
        this.platoPersistencePort = platoPersistencePort;
    }

    @Override
    public void savePlato(Plato plato,Long idUser) {
        plato.setActivo(true);
        platoPersistencePort.savePlato(plato,idUser);
    }

    @Override
    public void updatePlato(Plato plato,Long idUser) {
        platoPersistencePort.updatePlato(plato,idUser);
    }

    @Override
    public void updatePlatoActivo(Plato plato, Long idUser) {
        platoPersistencePort.updatePlatoActivo(plato,idUser);
    }

    @Override
    public Page<Plato> getAllPlatos(int numeroPagina, int size, Long idCategoriaOrdenar, Long idRestaurante,String campoOrdenar,String orden) {
        return platoPersistencePort.getAllPlatos(numeroPagina,size,idCategoriaOrdenar,idRestaurante,campoOrdenar,orden);
    }
}
