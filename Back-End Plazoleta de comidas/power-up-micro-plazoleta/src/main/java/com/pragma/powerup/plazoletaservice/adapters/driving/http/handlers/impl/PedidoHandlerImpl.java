package com.pragma.powerup.plazoletaservice.adapters.driving.http.handlers.impl;

import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.request.FinalizarPedidoReqDto;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.request.PedidoReqDto;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.response.PedidoResDto;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.response.PedidoTiempoResDto;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.response.RankingEmployeesResDto;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.handlers.IPedidoHandler;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.mapper.IPedidoResMapper;
import com.pragma.powerup.plazoletaservice.configuration.security.jwt.JwtService;
import com.pragma.powerup.plazoletaservice.domain.api.IPedidoServicePort;

import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class PedidoHandlerImpl implements IPedidoHandler {

    private final IPedidoServicePort pedidoServicePort;
    private final IPedidoResMapper pedidoResMapper;
    private final JwtService jwtService;
    private final HttpServletRequest request;
    @Override
    public void savePedido(PedidoReqDto pedidoReqDto) {
       pedidoServicePort.savePedido(pedidoReqDto,jwtService.getIdToken(request.getHeader("Authorization")));
    }

    @Override
    public Page<PedidoResDto> getPedidos(int numeroPagina, int size, Long idEstado, Long idRestaurante, String campoOrdenar, String orden) {
        return pedidoResMapper.toDtoPage(pedidoServicePort.getPedidos(numeroPagina,size,idEstado,idRestaurante,
                campoOrdenar,orden,jwtService.getIdToken((request.getHeader("Authorization")))));
    }

    @Override
    public void asignarPedido(Long idPedido) {
        pedidoServicePort.asignarPedido(idPedido,jwtService.getIdToken((request.getHeader("Authorization"))));
    }

    @Override
    public void pedidoListo(Long idPedido) {
        pedidoServicePort.pedidoListo(idPedido,jwtService.getIdToken((request.getHeader("Authorization"))));
    }

    @Override
    public void finalizarPedido(FinalizarPedidoReqDto finalizarPedidoReqDto) {
        pedidoServicePort.finalizarPedido(finalizarPedidoReqDto,jwtService.getIdToken((request.getHeader("Authorization"))));
    }

    @Override
    public void cancelarPedido(Long idPedido) {
        pedidoServicePort.cancelarPedido(idPedido,jwtService.getIdToken((request.getHeader("Authorization"))));
    }

    @Override
    public Page<PedidoTiempoResDto> getTiempoPedidos(int numeroPagina, int size, Long idRestaurante, String campoOrdenar, String orden) {
        return pedidoResMapper.toDtoTiempoPage(pedidoServicePort.getTiempoPedidos(numeroPagina,size,idRestaurante,campoOrdenar,orden,
                jwtService.getIdToken((request.getHeader("Authorization")))));
    }
    @Override
    public Page<RankingEmployeesResDto> getRankingEmployees(int numeroPagina, int size, Long idRestaurante) {
        return pedidoServicePort.getRankingEmployees(numeroPagina,size,idRestaurante,
                jwtService.getIdToken(request.getHeader("Authorization")));
    }
}
