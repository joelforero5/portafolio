package com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.repositories;

import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.entity.PedidoPlatoEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IPedidoPlatoRepository extends JpaRepository<PedidoPlatoEntity,Long> {
}
