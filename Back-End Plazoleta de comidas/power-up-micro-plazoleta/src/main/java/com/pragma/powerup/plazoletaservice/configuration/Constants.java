package com.pragma.powerup.plazoletaservice.configuration;

public class Constants {

    private Constants() {
        throw new IllegalStateException("Utility class");
    }

    public static final Long CLIENT_ROLE_ID = 1L;
    public static final Long EMPLOYEE_ROLE_ID = 2L;
    public static final Long PROVIDER_ROLE_ID = 3L;
    public static final int MAX_PAGE_SIZE = 2;
    public static final Long STATUS_ORDER_IN_PROGRESS_ID = 1L;
    public static final String STATUS_ORDER_IN_PROGRESS = "IN PROGRESS";
    public static final Long STATUS_ORDER_IN_PREPARATION_ID = 2L;
    public static final String STATUS_ORDER_IN_PREPARATION = "IN PREPARATION";
    public static final Long STATUS_ORDER_IN_PENDING_ID = 3L;
    public static final String STATUS_ORDER_PENDING = "PENDING";
    public static final Long STATUS_ORDER_IN_READY_ID = 4L;
    public static final String STATUS_ORDER_READY = "READY";
    public static final Long STATUS_ORDER_FINISHED = 5L;
    public static final String STATUS_ORDER_FINISHED_NAME = "FINISHED";
    public static final Long STATUS_ORDER_CANCELLED_ID = 6L;
    public static final String STATUS_ORDER_CANCELLED = "CANCELLED";
    public static final String RESPONSE_MESSAGE_KEY = "message";
    public static final String RESTAURANTE_CREATED_MESSAGE = "Restaurate Created Succesfully";
    public static final String ORDER_FINISHED_MESSAGE = "Pedido finalizado";
    public static final String ORDER_CANCELLED_MESSAGE = "Pedido Cancelado";
    public static final String ORDER_TOOK_MESSAGE = "Order Took Succesfully";
    public static final String ORDER_READY_MESSAGE = "Order is ready";
    public static final String EMPLOYEE_CREATED_MESSAGE = "Employee Created Succesfully";
    public static final String EMPLOYEE_NOT_CREATED_MESSAGE = "Employee Could not be Created";
    public static final String PLATO_CREATED_MESSAGE = "Plato Created Succesfully";
    public static final String ORDER_CREATED_MESSAGE = "Pedido Created Succesfully";
    public static final String ORDER_IS_ALREADY_TAKEN = "Pedido is already taken";
    public static final String ORDER_CANNOT_BE_TAKE = "Pedido cannot be take ";
    public static final String USER_CANT_MARK_READY_ORDER = "User can't mark this order as ready ";
    public static final String USER_CANT_MARK_FINISHED_ORDER = "User can't mark this order as finished ";
    public static final String CANT_MARK_FINISHED_ORDER = "Can't mark this order as finished ";
    public static final String USER_CANNOT_CANCEL_ORDER_ITS_NOT_OWNER = "User cannot cancel this order, its not the owner ";
    public static final String PIN_WRONG = "Pin code is wrong";
    public static final String USER_CANT_CANCEL_ORDER = "Sorry the order is already in preparation";
    public static final String RESPONSE_ERROR_MESSAGE_KEY = "error";
    public static final String WRONG_CREDENTIALS_MESSAGE = "Wrong credentials";
    public static final String UNAUTHORIZED_CREDENTIALS_MESSAGE = "Unauthorized credentials";
    public static final String USER_WITH_ORDER_IN_PROGRESS = "Client has already an order in progress";
    public static final String QUANTITY_DISH_INVALID = "Dish quantity cannot be negative";
    public static final String SOME_DISHES_ARE_NOT_FROM_RESTAURANT = "Some Dishes not belong to the restaurant";
    public static final String DISH_UPDATE_MESSAGE = "Plato Updated";
    public static final String NO_DATA_FOUND_MESSAGE = "No data found for the requested petition";
    public static final String PERSON_ALREADY_EXISTS_MESSAGE = "A person already exists with the DNI number provided";
    public static final String MAIL_ALREADY_EXISTS_MESSAGE = "A person with that mail already exists";
    public static final String PERSON_NOT_FOUND_MESSAGE = "No person found with the id provided";
    public static final String ROLE_NOT_FOUND_MESSAGE = "No role found with the id provided";
    public static final String ROLE_NOT_ALLOWED_MESSAGE = "No permission granted to create users with this role";
    public static final String CATEGORY_NOT_FOUND = "No Category found with the id provided";
    public static final String RESTAURANTE_NOT_FOUND = "No Restaurant found with the id provided";
    public static final String USER_ALREADY_EXISTS_MESSAGE = "A user already exists with the role provided";
    public static final String USER_NOT_FOUND_MESSAGE = "No user found with the role provided";
    public static final String SWAGGER_TITLE_MESSAGE = "User API Pragma Power Up";
    public static final String SWAGGER_DESCRIPTION_MESSAGE = "Plazoleta microservice";
    public static final String SWAGGER_VERSION_MESSAGE = "1.0.0";
    public static final String SWAGGER_LICENSE_NAME_MESSAGE = "Apache 2.0";
    public static final String SWAGGER_LICENSE_URL_MESSAGE = "http://springdoc.org";
    public static final String SWAGGER_TERMS_OF_SERVICE_MESSAGE = "http://swagger.io/terms/";
}
