package com.pragma.powerup.plazoletaservice.adapters.driving.http.client;

import com.pragma.powerup.plazoletaservice.configuration.security.feign.client.FeignClientConfig;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Map;

@FeignClient(name = "messenger-service", url = "http://localhost:8094",configuration = FeignClientConfig.class)
public interface IMessengerClient {
    @PostMapping("/messenger/sendMessage/{statusOrder}/{phoneNumber}")
    ResponseEntity<Map<String, String>> sendMessage(@PathVariable String statusOrder, @PathVariable String phoneNumber);
}
