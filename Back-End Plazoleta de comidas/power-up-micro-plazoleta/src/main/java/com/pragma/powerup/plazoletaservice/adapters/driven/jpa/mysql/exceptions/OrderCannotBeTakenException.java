package com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.exceptions;

public class OrderCannotBeTakenException extends RuntimeException{
    public OrderCannotBeTakenException(){super();}
}
