package com.pragma.powerup.plazoletaservice.domain.usecase.utils;

import java.util.concurrent.ThreadLocalRandom;

public class CodeGenerator {
    private CodeGenerator() {
    throw new IllegalStateException("Utility class");}

    private static final long MIN_RANGE=100_000;
    private static final long MAX_RANGE=999_999;
    public static String generateCode() {
        return String.valueOf(ThreadLocalRandom.current().nextLong(MIN_RANGE,MAX_RANGE)+1);
    }
}
