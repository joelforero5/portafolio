package com.pragma.powerup.plazoletaservice.adapters.driving.http.handlers.impl;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.request.EmployeeRequestDto;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.request.RestauranteRequestDto;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.response.RankingEmployeesResDto;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.response.RestauranteResponseDto;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.handlers.IRestauranteHandler;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.mapper.IEmpleadoRestauranteReqMapper;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.mapper.IRestauranteRequestMapper;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.mapper.IRestauranteResponseMappper;
import com.pragma.powerup.plazoletaservice.configuration.security.jwt.JwtService;
import com.pragma.powerup.plazoletaservice.domain.api.IRestauranteServicePort;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class RestauranteHandlerImpl implements IRestauranteHandler {
    private final IRestauranteServicePort restauranteServicePort;
    private final IRestauranteRequestMapper restauranteRequestMapper;
    private final IRestauranteResponseMappper restauranteResponseMappper;
    private final IEmpleadoRestauranteReqMapper empleadoRestauranteReqMapper;
    private final JwtService jwtService;
    private final HttpServletRequest request;
    @Override
    public void saveRestaurante(RestauranteRequestDto restauranteRequestDto) {
        restauranteServicePort.saveRestaurante(restauranteRequestMapper.toRestaurante(restauranteRequestDto));
    }

    @Override
    public Page<RestauranteResponseDto> getAllRestaurantes(int numeroPagina, int size) {
        return restauranteResponseMappper.toDtoPage(restauranteServicePort.getAllRestaurantes(numeroPagina,size));
    }

    @Override
    public void saveEmployee(EmployeeRequestDto employeeRequestDto) {
        restauranteServicePort.saveEmployee(employeeRequestDto,
                jwtService.getIdToken(request.getHeader("Authorization")));
    }

}
