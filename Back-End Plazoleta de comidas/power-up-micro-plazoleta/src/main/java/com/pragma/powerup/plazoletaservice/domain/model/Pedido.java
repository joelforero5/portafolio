package com.pragma.powerup.plazoletaservice.domain.model;

import jakarta.persistence.JoinColumn;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;
@Data
public class Pedido {
    private Long id;
    private Long idCliente;
    private Date fecha;
    private PedidoStatus idStatus;
    private EmpleadoRestaurante idChef;
    private Restaurante idRestaurante;
    private Double tiempoCompletado;
    private String pinPedido;

}
