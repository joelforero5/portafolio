package com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.mappers;

import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.entity.PedidoPlatoEntity;
import com.pragma.powerup.plazoletaservice.domain.model.Pedido;
import com.pragma.powerup.plazoletaservice.domain.model.PedidoPlato;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface IPedidoPlatoEntityMapper {
    List<PedidoPlatoEntity> toEntityList(List<PedidoPlato> pedidoPlatoList);
    PedidoPlatoEntity toEntity(PedidoPlato pedidoPlato);
}
