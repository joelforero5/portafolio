package com.pragma.powerup.plazoletaservice.configuration.security;

import com.pragma.powerup.plazoletaservice.configuration.security.jwt.JwtEntryPoint;
import com.pragma.powerup.plazoletaservice.configuration.security.jwt.JwtTokenFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class MainSecurity {
    private static final  String ROLE_OWNER="OWNER";
    private static final  String ROLE_ADMIN="ADMIN";
    private static final  String ROLE_EMPLOYEE="EMPLOYEE";
    private static final  String ROLE_CLIENT="CLIENT";

    @Autowired
    JwtEntryPoint jwtEntryPoint;

    @Bean
    public JwtTokenFilter jwtTokenFilter() {
        return new JwtTokenFilter();
    }

    @Bean
    public AuthenticationManager authenticationManager(
            AuthenticationConfiguration authConfig) throws Exception {
        return authConfig.getAuthenticationManager();
    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.cors().and().csrf().disable().authorizeHttpRequests(requests -> requests.requestMatchers("/actuator/health",
                                "/swagger-ui.html", "/swagger-ui/**", "/v3/api-docs/**","/restaurante/get-restaurantes").permitAll()
                        .requestMatchers("/restaurante/save").hasRole(ROLE_ADMIN)
                        .requestMatchers("/restaurante/employee/").hasRole(ROLE_OWNER)
                        .requestMatchers("/plato/**").hasRole(ROLE_OWNER)
                        .requestMatchers("/pedido/save-pedido").hasRole(ROLE_CLIENT)
                        .requestMatchers("/pedido/get-pedidos").hasRole(ROLE_EMPLOYEE)
                        .requestMatchers("/pedido/get-tiempo-pedidos").hasRole(ROLE_OWNER)
                        .requestMatchers("/pedido/pedido-listo/").hasRole(ROLE_EMPLOYEE)
                        .requestMatchers("/pedido/cancelar-pedido/").hasRole(ROLE_CLIENT)
                        .anyRequest().authenticated()).formLogin().and().httpBasic().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .exceptionHandling().authenticationEntryPoint(jwtEntryPoint);
        http.addFilterBefore(jwtTokenFilter(), UsernamePasswordAuthenticationFilter.class);
        return http.build();
    }
}
