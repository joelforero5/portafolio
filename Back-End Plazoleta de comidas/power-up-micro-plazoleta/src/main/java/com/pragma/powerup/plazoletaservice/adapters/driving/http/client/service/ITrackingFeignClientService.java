package com.pragma.powerup.plazoletaservice.adapters.driving.http.client.service;

import com.pragma.powerup.plazoletaservice.domain.model.Tracking;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;

public interface ITrackingFeignClientService {
    ResponseEntity<Map<String, String>> trackingOrder(Tracking tracking);
}
