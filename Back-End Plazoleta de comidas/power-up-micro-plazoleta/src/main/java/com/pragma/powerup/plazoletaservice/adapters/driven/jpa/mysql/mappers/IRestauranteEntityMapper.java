package com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.mappers;

import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.entity.RestauranteEntity;
import com.pragma.powerup.plazoletaservice.domain.model.Restaurante;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface IRestauranteEntityMapper {
    IRestauranteEntityMapper INSTANCE= Mappers.getMapper(IRestauranteEntityMapper.class);
    RestauranteEntity toEntity(Restaurante restaurante);
    Restaurante toRestaurante(RestauranteEntity restauranteEntity);
    List<Restaurante> toRestauranteList(List<RestauranteEntity> restauranteEntityList);

    default Page<Restaurante> toRestaurantePage(Page<RestauranteEntity> page) {
        List<Restaurante> restauranteList = toRestauranteList(page.getContent());
        return new PageImpl<>(restauranteList, page.getPageable(), page.getTotalElements());
    }
}
