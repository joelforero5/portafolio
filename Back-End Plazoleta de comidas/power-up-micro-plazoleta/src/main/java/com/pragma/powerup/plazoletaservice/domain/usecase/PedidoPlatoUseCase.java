package com.pragma.powerup.plazoletaservice.domain.usecase;

import com.pragma.powerup.plazoletaservice.domain.api.IPedidoPlatoServicePort;
import com.pragma.powerup.plazoletaservice.domain.spi.IPedidoPlatoPersistencePort;

public class PedidoPlatoUseCase implements IPedidoPlatoServicePort {
    private final IPedidoPlatoPersistencePort pedidoPlatoPersistencePort;

    public PedidoPlatoUseCase(IPedidoPlatoPersistencePort pedidoPlatoPersistencePort) {
        this.pedidoPlatoPersistencePort = pedidoPlatoPersistencePort;
    }
}
