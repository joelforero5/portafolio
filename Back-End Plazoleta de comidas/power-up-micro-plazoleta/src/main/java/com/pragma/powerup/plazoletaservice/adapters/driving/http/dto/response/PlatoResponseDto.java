package com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.response;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class PlatoResponseDto {
    private String nombre;
    private String descripcion;
    private Long precio;
    private String urlImagen;
}
