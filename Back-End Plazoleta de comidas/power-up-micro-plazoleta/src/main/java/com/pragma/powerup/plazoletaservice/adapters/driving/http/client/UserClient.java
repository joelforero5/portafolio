package com.pragma.powerup.plazoletaservice.adapters.driving.http.client;


import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.request.EmployeeRequestDto;
import com.pragma.powerup.plazoletaservice.configuration.security.feign.client.FeignClientConfig;
import jakarta.validation.Valid;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;

@FeignClient(name = "user-service",configuration = FeignClientConfig.class)
public interface UserClient {
    @GetMapping("/users/get-owner/{id}")
    ResponseEntity<Boolean> getOwner(@PathVariable Long id);
    @GetMapping("/users/get-client/{id}")
    ResponseEntity<Boolean> getClient(@PathVariable Long id);
    @GetMapping("/users/get-employee/{id}")
    ResponseEntity<Boolean> getEmployee(@PathVariable Long id);

}
