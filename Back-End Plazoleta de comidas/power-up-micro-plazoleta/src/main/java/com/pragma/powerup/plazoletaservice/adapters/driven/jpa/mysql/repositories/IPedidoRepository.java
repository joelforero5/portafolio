package com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.repositories;

import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.entity.PedidoEntity;
import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.entity.PedidoStatusEntity;
import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.entity.RestauranteEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface IPedidoRepository extends JpaRepository<PedidoEntity,Long> {
    Page<PedidoEntity> findByIdStatus(PedidoStatusEntity idStatus, Pageable pageable);
    Optional<PedidoEntity> findFirstByIdClienteAndIdStatusIn(Long idCliente, List<PedidoStatusEntity> idStatusList);

    Page<PedidoEntity> findByIdRestauranteAndTiempoCompletadoIsNotNull(RestauranteEntity restaurante, Pageable pageable);
    @Query(value = "SELECT p.id_chef, AVG(p.tiempo_completado) AS tiempoPromedio " +
            "FROM pedidos p " +
            "WHERE p.id_restaurante = ?1 " +
            "GROUP BY p.id_chef " +
            "ORDER BY tiempoPromedio DESC", nativeQuery = true)
    Page<Object[]> obtenerRankingEmployeesPorTiempoPromedio(@Param("id_restaurante") Long idRestaurante, Pageable pageable);

}
