package com.pragma.powerup.plazoletaservice.domain.model;

public class PedidoPlato {
    private Long id;
    private Pedido idPedido;
    private Plato idPlato;
    private Long cantidad;

    public PedidoPlato(Long id, Pedido idPedido, Plato idPlato, Long cantidad) {
        this.id = id;
        this.idPedido = idPedido;
        this.idPlato = idPlato;
        this.cantidad = cantidad;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Pedido getIdPedido() {
        return idPedido;
    }

    public void setIdPedido(Pedido idPedido) {
        this.idPedido = idPedido;
    }

    public Plato getIdPlato() {
        return idPlato;
    }

    public void setIdPlato(Plato idPlato) {
        this.idPlato = idPlato;
    }

    public Long getCantidad() {
        return cantidad;
    }

    public void setCantidad(Long cantidad) {
        this.cantidad = cantidad;
    }
}

