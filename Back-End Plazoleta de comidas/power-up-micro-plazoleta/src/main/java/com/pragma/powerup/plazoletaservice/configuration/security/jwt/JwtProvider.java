package com.pragma.powerup.plazoletaservice.configuration.security.jwt;


import io.jsonwebtoken.*;


import io.jsonwebtoken.security.Keys;
import io.jsonwebtoken.security.SignatureException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.security.Key;
import java.util.Collection;
import java.util.Collections;



@Component
public class JwtProvider implements JwtService{
    private final static Logger logger = LoggerFactory.getLogger(JwtProvider.class);
    @Value("${jwt.secret}")
    private String secret;
    @Value("${jwt.expiration}")
    private int expiration;

    public boolean validate(String token) {
        try {
            Jwts.parserBuilder().setSigningKey(secret.getBytes()).build().parseClaimsJws(token);
            return true;
        } catch (JwtException | IllegalArgumentException e) {
            return false;
        }
    }
    public Collection<? extends GrantedAuthority> getAuthorities(String token) {
        Claims auth = Jwts.parserBuilder().setSigningKey(secret.getBytes()).build().parseClaimsJws(token).getBody();
        return Collections.singletonList(new SimpleGrantedAuthority(auth.get("roles").toString().replace("[","").replace("]","")));
    }

    @Override
    public Long getIdToken(String token) {
        token = token.replace("Bearer ","");
        Claims claims = Jwts.parser().setSigningKey(secret.getBytes()).parseClaimsJws(token).getBody();
        return claims.get("id", Long.class);
    }

}