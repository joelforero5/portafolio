package com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class PlatoUpdateReqDto {
    @Schema(accessMode = Schema.AccessMode.READ_ONLY)
    @NotNull
    private Long id;
    @NotBlank
    private String descripcion;
    @NotNull
    @Positive
    private Long precio;
}
