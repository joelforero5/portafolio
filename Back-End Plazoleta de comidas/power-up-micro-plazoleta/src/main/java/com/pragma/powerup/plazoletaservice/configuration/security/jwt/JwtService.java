package com.pragma.powerup.plazoletaservice.configuration.security.jwt;

public interface JwtService {
    Long getIdToken(String token);
}
