package com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.adapter;

import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.entity.RestauranteEntity;
import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.mappers.IEmpleadoRestauranteEntityMapper;
import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.mappers.IRestauranteEntityMapper;
import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.repositories.IEmpleadoRestauranteRepository;
import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.repositories.IRestauranteRepository;
import com.pragma.powerup.plazoletaservice.domain.model.EmpleadoRestaurante;
import com.pragma.powerup.plazoletaservice.domain.model.Restaurante;
import com.pragma.powerup.plazoletaservice.domain.spi.IRestaurantePersistencePort;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.annotation.Transactional;

@RequiredArgsConstructor
@Transactional
public class RestauranteMysqlAdapter implements IRestaurantePersistencePort {
    private final IRestauranteRepository restauranteRepository;
    private final IRestauranteEntityMapper restauranteEntityMapper;
    private final IEmpleadoRestauranteRepository empleadoRestauranteRepository;
    private final IEmpleadoRestauranteEntityMapper empleadoRestauranteEntityMapper;


    @Override
    public void saveRestaurante(Restaurante restaurante) {
        restauranteRepository.save(restauranteEntityMapper.toEntity(restaurante));
    }

    @Override
    public Page<Restaurante> getAllRestaurantesPage(int numeroPagina, int size) {
        Sort sort = Sort.by(Sort.Direction.ASC,"nombre");
        Pageable pageable = PageRequest.of(numeroPagina,size,sort);
        Page<RestauranteEntity> restauranteEntities = restauranteRepository.findAll(pageable);
        return restauranteEntityMapper.toRestaurantePage(restauranteEntities);
    }

    @Override
    public void saveEmployee(EmpleadoRestaurante empleadoRestaurante) {
        empleadoRestauranteRepository.save(empleadoRestauranteEntityMapper.toEntity(empleadoRestaurante));
    }

    @Override
    public boolean existsByIdAndIdPropietario(Long idRestaurante, Long idUser) {
        return restauranteRepository.existsByIdAndIdPropietario(idRestaurante,idUser);
    }

    @Override
    public boolean existsEmployeeByIdAndIdRestaurante(Long idEmpleado, Restaurante idRestaurante) {
        return empleadoRestauranteRepository.existsByIdEmpleadoAndIdRestaurante(idEmpleado,restauranteEntityMapper.toEntity(idRestaurante));
    }


}
