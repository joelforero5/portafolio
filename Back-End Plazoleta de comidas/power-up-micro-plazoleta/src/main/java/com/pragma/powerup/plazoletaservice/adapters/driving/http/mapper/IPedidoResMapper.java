package com.pragma.powerup.plazoletaservice.adapters.driving.http.mapper;

import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.response.PedidoResDto;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.response.PedidoTiempoResDto;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.response.PlatoResponseDto;
import com.pragma.powerup.plazoletaservice.domain.model.Pedido;
import com.pragma.powerup.plazoletaservice.domain.model.Plato;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;

@Mapper(componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface IPedidoResMapper {
    @Mapping(target = "idStatus",source = "idStatus.name")
    @Mapping(target = "idRestaurante",source = "idRestaurante.nombre")
    @Mapping(target = "idChef",source = "idChef.idEmpleado")
    PedidoResDto toDto(Pedido pedido);
    @Mapping(target = "idStatus",source = "idStatus.name")
    @Mapping(target = "idRestaurante",source = "idRestaurante.nombre")
    @Mapping(target = "idChef",source = "idChef.idEmpleado")
    List<PedidoResDto> toDtoList(List<Pedido> pedidoList);

    @Mapping(target = "idChef",source = "idChef.idEmpleado")
    PedidoTiempoResDto toTiempoDto(Pedido pedido);
    @Mapping(target = "idChef.idEmpleado",source ="idChef")
    Pedido toPedido(PedidoTiempoResDto pedidoTiempoResDto);
    List<PedidoTiempoResDto> toDtoTiempoList(List<Pedido> pedidoList);

    default Page<PedidoResDto> toDtoPage(Page<Pedido> page) {
        List<PedidoResDto> responseDtos = toDtoList(page.getContent());
        return new PageImpl<>(responseDtos, page.getPageable(), page.getTotalElements());
    }
    default Page<PedidoTiempoResDto> toDtoTiempoPage(Page<Pedido> page) {
        List<PedidoTiempoResDto> responseDtos = toDtoTiempoList(page.getContent());
        return new PageImpl<>(responseDtos, page.getPageable(), page.getTotalElements());
    }
}
