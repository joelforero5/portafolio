package com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;


@AllArgsConstructor
@Getter
public class EmployeeRequestDto {
    @Schema(title = "id_empleado", description = "id_empleado", example = "1L")
    @NotNull
    private Long idEmpleado;
    @Schema(title = "id_restaurante", description = "id_restaurante", example = "1L")
    @NotNull
    private Long idRestaurante;

}
