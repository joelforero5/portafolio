package com.pragma.powerup.plazoletaservice.adapters.driving.http.controller;

import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.request.PlatoEnableDisableRequestDto;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.request.PlatoRequestDto;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.request.PlatoUpdateReqDto;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.request.RestauranteRequestDto;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.response.PlatoResponseDto;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.response.RestauranteResponseDto;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.handlers.IPlatoHandler;
import com.pragma.powerup.plazoletaservice.configuration.Constants;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.Map;

import static com.pragma.powerup.plazoletaservice.configuration.Constants.DISH_UPDATE_MESSAGE;

@RestController
@RequestMapping("/plato")
@RequiredArgsConstructor
@SecurityRequirement(name = "jwt")
public class PlatoController {
    private final IPlatoHandler platoHandler;
    @Operation(summary = "Add a new Plato",
            responses = {
                    @ApiResponse(responseCode = "201", description = "Plato created",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Map"))),
                    @ApiResponse(responseCode = "409", description = "Plato already exists",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Error"))),
                    @ApiResponse(responseCode = "403", description = "Plato not allowed for creation",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Error")))})
    @PostMapping("/save-plato")
    //@PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Map<String, String>> savePlato(@Valid @RequestBody PlatoRequestDto platoRequestDto) {
        platoHandler.savePlato(platoRequestDto);
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(Collections.singletonMap(Constants.RESPONSE_MESSAGE_KEY, Constants.PLATO_CREATED_MESSAGE));
    }
    @PutMapping("/{id}")
    public ResponseEntity<Map<String, String>> uptadePlato(@PathVariable Long id, @RequestBody PlatoUpdateReqDto platoUpdateReqDto) {
        platoUpdateReqDto.setId(id);
        platoHandler.updatePlato(platoUpdateReqDto);
        return ResponseEntity.status(HttpStatus.OK)
                .body(Collections.singletonMap(Constants.RESPONSE_MESSAGE_KEY, DISH_UPDATE_MESSAGE));
    }
    @PutMapping("/enable-disable/{id}")
    public ResponseEntity<Map<String, String>> enableDisablePlato(@PathVariable Long id, @RequestBody PlatoEnableDisableRequestDto platoEnableDisableRequestDto) {
        platoEnableDisableRequestDto.setId(id);
        platoHandler.updatePlatoActivo(platoEnableDisableRequestDto);
        return ResponseEntity.status(HttpStatus.OK)
                .body(Collections.singletonMap(Constants.RESPONSE_MESSAGE_KEY, DISH_UPDATE_MESSAGE));
    }
    @Operation(summary = "Get all platos",
            responses = {
                    @ApiResponse(responseCode = "200", description = "All platos returned"),
                    @ApiResponse(responseCode = "404", description = "platos not found ",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Error")))})
    @GetMapping("/get-platos")
    public ResponseEntity<Page<PlatoResponseDto>> getAllPlatos(@RequestParam int numeroPagina,
                                                               @RequestParam int size,
                                                               @RequestParam(required = false) Long idCategoriaOrdenar,
                                                               @RequestParam Long idRestaurante,
                                                               @RequestParam(defaultValue = "nombre")String campoOrdenar,
                                                               @RequestParam(defaultValue = "ASC")String orden) {
        return ResponseEntity.ok(platoHandler.getAllPlatos(numeroPagina,size,idCategoriaOrdenar,idRestaurante,campoOrdenar,orden));
    }

}
