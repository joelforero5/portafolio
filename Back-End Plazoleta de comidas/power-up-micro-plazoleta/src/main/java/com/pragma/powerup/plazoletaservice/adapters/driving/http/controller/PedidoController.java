package com.pragma.powerup.plazoletaservice.adapters.driving.http.controller;

import com.netflix.discovery.converters.Auto;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.request.FinalizarPedidoReqDto;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.request.PedidoReqDto;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.request.PlatoRequestDto;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.response.PedidoResDto;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.response.PedidoTiempoResDto;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.response.PlatoResponseDto;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.response.RankingEmployeesResDto;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.handlers.IPedidoHandler;
import com.pragma.powerup.plazoletaservice.configuration.Constants;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.Map;
@RestController
@RequestMapping("/pedido")
@RequiredArgsConstructor
@SecurityRequirement(name = "jwt")
public class PedidoController {
    @Autowired
    private IPedidoHandler pedidoHandler;
    @Operation(summary = "Create Pedido",
            responses = {
                    @ApiResponse(responseCode = "201", description = "Pedido created",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Map"))),
                    @ApiResponse(responseCode = "403", description = "Plato not allowed for creation",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Error")))})
    @PostMapping("/save-pedido")
    public ResponseEntity<Map<String, String>> savePlato(@Valid @RequestBody PedidoReqDto pedidoReqDto) {
        pedidoHandler.savePedido(pedidoReqDto);
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(Collections.singletonMap(Constants.RESPONSE_MESSAGE_KEY, Constants.ORDER_CREATED_MESSAGE));
    }
    @Operation(summary = "Get  Pedidos",
            responses = {
                    @ApiResponse(responseCode = "200", description = "All Pedidos returned"),
                    @ApiResponse(responseCode = "404", description = "Pedidos not found ",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Error")))})
    @GetMapping("/get-pedidos")
    public ResponseEntity<Page<PedidoResDto>> getAllPlatos(@RequestParam int numeroPagina,
                                                           @RequestParam int size,
                                                           @RequestParam Long idEstado,
                                                           @RequestParam Long idRestaurante,
                                                           @RequestParam(defaultValue = "fecha")String campoOrdenar,
                                                           @RequestParam(defaultValue = "ASC")String orden) {
        return ResponseEntity.ok(pedidoHandler.getPedidos(numeroPagina,size,idEstado,idRestaurante,campoOrdenar,orden));
    }
    @Operation(summary = "Get tiempo Pedidos",
            responses = {
                    @ApiResponse(responseCode = "200", description = "All tiempos de Pedidos returned"),
                    @ApiResponse(responseCode = "404", description = "Pedidos not found ",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Error")))})
    @GetMapping("/get-tiempo-pedidos")
    public ResponseEntity<Page<PedidoTiempoResDto>> getAllPlatos(@RequestParam int numeroPagina,
                                                                 @RequestParam int size,
                                                                 @RequestParam Long idRestaurante,
                                                                 @RequestParam(defaultValue = "fecha")String campoOrdenar,
                                                                 @RequestParam(defaultValue = "ASC")String orden) {
        return ResponseEntity.ok(pedidoHandler.getTiempoPedidos(numeroPagina,size,idRestaurante,campoOrdenar,orden));
    }
    @Operation(summary = "Get ranking employees",
            responses = {
                    @ApiResponse(responseCode = "200", description = "ranking employees returned"),
                    @ApiResponse(responseCode = "404", description = "ranking employees not found ",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Error")))})
    @GetMapping("/get-ranking-employess")
    public ResponseEntity<Page<RankingEmployeesResDto>> getRankingEmployees(@RequestParam int numeroPagina, @RequestParam int size, @RequestParam Long idRestaurante) {
        return ResponseEntity.ok(pedidoHandler.getRankingEmployees(numeroPagina,size,idRestaurante));
    }
    @Operation(summary = "Take a new order",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Order took",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Map"))),
                    @ApiResponse(responseCode = "409", description = "error creating order",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Error")))})
    @PostMapping("/asignar-pedido/{idPedido}")
    @SecurityRequirement(name = "jwt")
    public ResponseEntity<Map<String, String>> takeOrder(@PathVariable Long idPedido) {
        pedidoHandler.asignarPedido(idPedido);
        return ResponseEntity.status(HttpStatus.OK)
                .body(Collections.singletonMap(Constants.RESPONSE_MESSAGE_KEY, Constants.ORDER_TOOK_MESSAGE));
    }
    @Operation(summary = "Mark order as ready",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Order ready",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Map"))),
                    @ApiResponse(responseCode = "409", description = "error marking order as ready",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Error")))})
    @PostMapping("/pedido-listo/{idPedido}")
    @SecurityRequirement(name = "jwt")
    public ResponseEntity<Map<String, String>> pedidoListo(@PathVariable Long idPedido) {
        pedidoHandler.pedidoListo(idPedido);
        return ResponseEntity.status(HttpStatus.OK)
                .body(Collections.singletonMap(Constants.RESPONSE_MESSAGE_KEY, Constants.ORDER_READY_MESSAGE));
    }
    @Operation(summary = "Mark order as finished",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Order finished",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Map"))),
                    @ApiResponse(responseCode = "409", description = "error marking order as finished",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Error")))})
    @PostMapping("/finalizar-pedido/")
    @SecurityRequirement(name = "jwt")
    public ResponseEntity<Map<String, String>> finalizarPedido(@Valid @RequestBody FinalizarPedidoReqDto finalizarPedidoReqDto) {
        pedidoHandler.finalizarPedido(finalizarPedidoReqDto);
        return ResponseEntity.status(HttpStatus.OK)
                .body(Collections.singletonMap(Constants.RESPONSE_MESSAGE_KEY, Constants.ORDER_FINISHED_MESSAGE));
    }
    @Operation(summary = "Cancel order",
            responses = {
                    @ApiResponse(responseCode = "200", description = "Order cancelled",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Map"))),
                    @ApiResponse(responseCode = "409", description = "error marking order as finished",
                            content = @Content(mediaType = "application/json", schema = @Schema(ref = "#/components/schemas/Error")))})
    @PostMapping("/cancelar-pedido/{idPedido}")
    @SecurityRequirement(name = "jwt")
    public ResponseEntity<Map<String, String>> cancelarPedido(@PathVariable Long idPedido) {
        pedidoHandler.cancelarPedido(idPedido);
        return ResponseEntity.status(HttpStatus.OK)
                .body(Collections.singletonMap(Constants.RESPONSE_MESSAGE_KEY, Constants.ORDER_CANCELLED_MESSAGE));
    }
}
