package com.pragma.powerup.plazoletaservice.domain.usecase;

import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.entity.EmpleadoRestauranteEntity;
import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.entity.PedidoEntity;
import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.entity.PedidoStatusEntity;
import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.entity.RestauranteEntity;
import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.exceptions.UnauthorizedUserException;
import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.exceptions.UserNotFoundException;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.assets.PlatoAsset;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.client.ITrackingFeignClient;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.client.service.IMessengerClientService;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.client.service.ITrackingFeignClientService;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.client.service.IUserClientService;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.request.FinalizarPedidoReqDto;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.request.PedidoReqDto;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.response.RankingEmployeesResDto;
import com.pragma.powerup.plazoletaservice.domain.exceptions.*;
import com.pragma.powerup.plazoletaservice.domain.model.*;
import com.pragma.powerup.plazoletaservice.domain.spi.IPedidoPersistencePort;
import com.pragma.powerup.plazoletaservice.domain.spi.IPedidoPlatoPersistencePort;
import com.pragma.powerup.plazoletaservice.domain.spi.IPlatoPersistencePort;

import com.pragma.powerup.plazoletaservice.domain.spi.IRestaurantePersistencePort;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.*;

import static com.pragma.powerup.plazoletaservice.configuration.Constants.*;
import static com.pragma.powerup.plazoletaservice.utils.PlatoModeloValues.INSTANCE_DISH;
import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.mockito.Mockito.*;

class PedidoUseCaseTest {
    @Mock
    private IPedidoPersistencePort pedidoPersistencePort;
    @Mock
    private IUserClientService userClientService;
    @Mock
    private ITrackingFeignClientService trackingFeignClientService;
    @Mock
    private IMessengerClientService messengerClientService;
    @Mock
    private IPlatoPersistencePort platoPersistencePort;
    @Mock
    private IPedidoPlatoPersistencePort pedidoPlatoPersistencePort;
    @Mock
    private IRestaurantePersistencePort restaurantePersistencePort;

    private PedidoUseCase pedidoUseCase;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        pedidoUseCase = new PedidoUseCase(pedidoPersistencePort, userClientService, trackingFeignClientService, platoPersistencePort, pedidoPlatoPersistencePort,restaurantePersistencePort, messengerClientService);
    }

    @Test
    void savePedido_WhenUserDoesNotExist_ShouldThrowUserNotFoundException() {
        // Arrange
        Long idUser = 1L;
        PedidoReqDto pedidoReqDto = new PedidoReqDto();
        when(userClientService.existsClient(idUser)).thenReturn(false);

        // Act & Assert
        assertThrows(UserNotFoundException.class, () -> pedidoUseCase.savePedido(pedidoReqDto, idUser));
    }

    @Test
    void savePedido_WhenInvalidDishQuantity_ShouldThrowQuantityDishInvalidException() {
        // Arrange
        Long idUser = 1L;
        PedidoReqDto pedidoReqDto = new PedidoReqDto();
        pedidoReqDto.setPlatos(new ArrayList<>());
        PlatoAsset platoAsset = new PlatoAsset();
        platoAsset.setIdPlato(1L);
        platoAsset.setCantidad(0L);
        pedidoReqDto.getPlatos().add(platoAsset);
        Restaurante idRestaurante = new Restaurante(1L,null,null,null,null,null,null);
        Pedido pedido = new Pedido();
        pedido.setIdCliente(idUser);
        pedido.setFecha(new Date());
        pedido.setIdStatus(new PedidoStatus(STATUS_ORDER_IN_PENDING_ID,null,null));
        pedido.setIdRestaurante(idRestaurante);
        when(pedidoPersistencePort.savePedido(pedido)).thenReturn(1L);
        Long idPedido = pedidoPersistencePort.savePedido(pedido);
        pedido.setId(idPedido);
        when(platoPersistencePort.existPlatoByIdAndIdRestaurante(platoAsset.getIdPlato(),idRestaurante)).thenReturn(true);
        assertThrows(QuantityDishInvalidException.class, () -> pedidoUseCase.validateDishesToSave(pedidoReqDto.getPlatos(),idRestaurante,pedido));

        // Act & Assert

    }

    @Test
    void savePedido_WhenSomeDishesAreNotFromRestaurant_ShouldThrowSomeDishesAreNotFromRestaurantException() {
        // Arrange
        // Arrange
        Long idUser = 1L;
        PedidoReqDto pedidoReqDto = new PedidoReqDto();
        pedidoReqDto.setPlatos(new ArrayList<>());
        PlatoAsset platoAsset = new PlatoAsset();
        platoAsset.setIdPlato(1L);
        platoAsset.setCantidad(2L);
        pedidoReqDto.getPlatos().add(platoAsset);
        Restaurante idRestaurante = new Restaurante(1L,null,null,null,null,null,null);
        Pedido pedido = new Pedido();
        pedido.setIdCliente(idUser);
        pedido.setFecha(new Date());
        pedido.setIdStatus(new PedidoStatus(STATUS_ORDER_IN_PENDING_ID,null,null));
        pedido.setIdRestaurante(idRestaurante);
        when(pedidoPersistencePort.savePedido(pedido)).thenReturn(1L);
        Long idPedido = pedidoPersistencePort.savePedido(pedido);
        pedido.setId(idPedido);
        when(platoPersistencePort.existPlatoByIdAndIdRestaurante(platoAsset.getIdPlato(),idRestaurante)).thenReturn(false);
        assertThrows(SomeDishesAreNotFromRestaurantException.class, () -> pedidoUseCase.validateDishesToSave(pedidoReqDto.getPlatos(),idRestaurante,pedido));
    }
    @Test
    void savePedido_WhenAllConditionsAreMet_ShouldSavePedidoSuccessfully() {
        Long idUser = 1L;
        Long idPedido = 1L;
        PedidoReqDto pedidoReqDto = new PedidoReqDto();
        pedidoReqDto.setIdRestaurante(1L);
        pedidoReqDto.setPlatos(new ArrayList<>());
        PlatoAsset platoAsset = new PlatoAsset();
        platoAsset.setIdPlato(1L);
        platoAsset.setCantidad(1L);
        pedidoReqDto.getPlatos().add(platoAsset);
        PedidoEntity pedido = new PedidoEntity();
        pedido.setId(idPedido);
        pedido.setIdCliente(idUser);
        pedido.setFecha(new Date());
        EmpleadoRestauranteEntity empleadoRestaurante = new EmpleadoRestauranteEntity();
        empleadoRestaurante.setIdEmpleado(1L);
        pedido.setIdChef(empleadoRestaurante);
        pedido.setIdStatus(new PedidoStatusEntity(STATUS_ORDER_IN_PENDING_ID,null,null));
        RestauranteEntity restaurante = new RestauranteEntity();
        restaurante.setId(pedidoReqDto.getIdRestaurante());
        pedido.setIdRestaurante(restaurante);

        when(pedidoPersistencePort.findPedidoById(idPedido)).thenReturn(pedido);
        when(userClientService.existsClient(idUser)).thenReturn(true);
        when(platoPersistencePort.existPlatoByIdAndIdRestaurante(anyLong(), any())).thenReturn(true);
        when(platoPersistencePort.findDishById(anyLong())).thenReturn(INSTANCE_DISH);
        when(pedidoPersistencePort.savePedido(any())).thenReturn(idPedido);
        when(pedidoPersistencePort.toPedidoEntity(any())).thenReturn(pedido);

        // Act
        pedidoUseCase.savePedido(pedidoReqDto, idUser);

        // Assert
        verify(pedidoPersistencePort, times(1)).savePedido(any());
        verify(pedidoPlatoPersistencePort, times(1)).saveOrderDishes(any());
    }

    @Test
    void getPedidos_WithValidData_ShouldReturnPageOfPedidos() {
        // Arrange
        int numeroPagina = 0;
        int size = 10;
        long idEstado = 1L;
        long idRestaurante = 2L;
        String campoOrdenar = "nombre";
        String orden = "asc";
        long authorization = 3L;

        Restaurante restaurante = new Restaurante(idRestaurante, null, null, null, null, null, null);
        Page<Pedido> pedidos = Mockito.mock(Page.class);

        when(restaurantePersistencePort.existsEmployeeByIdAndIdRestaurante(eq(authorization), any(Restaurante.class))).thenReturn(true);
        when(pedidoPersistencePort.getPedidos(eq(numeroPagina), eq(size), eq(idEstado), eq(idRestaurante), eq(campoOrdenar), eq(orden))).thenReturn(pedidos);

        // Act
        Page<Pedido> result = pedidoUseCase.getPedidos(numeroPagina, size, idEstado, idRestaurante, campoOrdenar, orden, authorization);

        // Assert
        assertNotNull(result);
        assertEquals(pedidos, result);
    }

    @Test
    void getPedidos_WithInvalidParameters_ShouldThrowParametersNegativesException() {
        // Arrange
        int numeroPagina = -1;
        int size = 10;
        long idEstado = 1L;
        long idRestaurante = 2L;
        String campoOrdenar = "nombre";
        String orden = "asc";
        long authorization = 3L;

        // Act & Assert
        assertThrows(ParametersNegativesException.class,
                () -> pedidoUseCase.getPedidos(numeroPagina, size, idEstado, idRestaurante, campoOrdenar, orden, authorization));
    }

    @Test
    void getPedidos_WithUnauthorizedUser_ShouldThrowUnauthorizedUserException() {
        // Arrange
        int numeroPagina = 0;
        int size = 10;
        long idEstado = 1L;
        long idRestaurante = 2L;
        String campoOrdenar = "nombre";
        String orden = "asc";
        long authorization = 3L;

        Restaurante restaurante = new Restaurante(idRestaurante, null, null, null, null, null, null);

        when(restaurantePersistencePort.existsEmployeeByIdAndIdRestaurante(eq(authorization), any(Restaurante.class))).thenReturn(false);

        // Act & Assert
        assertThrows(UnauthorizedUserException.class,
                () -> pedidoUseCase.getPedidos(numeroPagina, size, idEstado, idRestaurante, campoOrdenar, orden, authorization));
    }
    @Test
    void asignarPedido_ValidIdParameters_CallsPersistencePort() {
        // Arrange
        Long idPedido = 1L;
        Long idUser = 2L;
        PedidoEntity pedido = new PedidoEntity();
        pedido.setId(idPedido);
        pedido.setIdCliente(idUser);
        pedido.setFecha(new Date());
        EmpleadoRestauranteEntity empleadoRestaurante = new EmpleadoRestauranteEntity();
        empleadoRestaurante.setIdEmpleado(1L);
        pedido.setIdChef(empleadoRestaurante);
        pedido.setIdStatus(new PedidoStatusEntity(STATUS_ORDER_IN_PENDING_ID,null,null));
        RestauranteEntity restaurante = new RestauranteEntity();
        restaurante.setId(1L);
        pedido.setIdRestaurante(restaurante);
        when(pedidoPersistencePort.findPedidoById(idPedido)).thenReturn(pedido);
        // Act
        pedidoUseCase.asignarPedido(idPedido, idUser);

        // Assert
        verify(pedidoPersistencePort).asignarPedido(idPedido, idUser);
    }

    @Test
    void asignarPedido_NegativeIdPedido_ThrowsParametersNegativesException() {
        // Arrange
        Long idPedido = -1L;
        Long idUser = 2L;

        // Assert
        assertThrows(ParametersNegativesException.class, () -> {
            // Act
            pedidoUseCase.asignarPedido(idPedido, idUser);
        });
    }
    @Test
    void pedidoListo_NoExceptions_OrderIsMarkedAsReady() {
        // Arrange
        Long idPedido = 1L;
        Long authorization = 2L;
        PedidoEntity pedidoEntity = new PedidoEntity();
        PedidoStatusEntity pedidoStatus = new PedidoStatusEntity(STATUS_ORDER_IN_PREPARATION_ID,STATUS_ORDER_IN_PREPARATION,STATUS_ORDER_IN_PREPARATION);
        pedidoEntity.setIdStatus(pedidoStatus);
        EmpleadoRestauranteEntity empleadoRestaurante = new EmpleadoRestauranteEntity();
        empleadoRestaurante.setIdEmpleado(authorization);
        pedidoEntity.setIdChef(empleadoRestaurante);
        RestauranteEntity restaurante = new RestauranteEntity();
        restaurante.setId(1L);
        pedidoEntity.setIdRestaurante(restaurante);

        // Stubbing
        when(pedidoPersistencePort.findPedidoById(idPedido)).thenReturn(pedidoEntity);

        // Act
        pedidoUseCase.pedidoListo(idPedido, authorization);

        // Assert
        verify(pedidoPersistencePort).pedidoListo(pedidoEntity);
    }

    @Test
    void pedidoListo_InvalidIdPedido_ThrowsParametersNegativesException() {
        // Arrange
        Long idPedido = -1L;
        Long authorization = 2L;

        // Act & Assert
        assertThrows(ParametersNegativesException.class, () -> {
            pedidoUseCase.pedidoListo(idPedido, authorization);
        });

        // Verify that pedidoPersistencePort.pedidoListo() was not called
        verify(pedidoPersistencePort, never()).pedidoListo(any());
    }

    @Test
    void pedidoListo_OrderNotInPreparation_ThrowsCantMarkOrderReadyException() {
        // Arrange
        Long idPedido = 1L;
        Long authorization = 2L;
        PedidoEntity pedidoEntity = new PedidoEntity();
        PedidoStatusEntity pedidoStatus = new PedidoStatusEntity(STATUS_ORDER_IN_READY_ID,STATUS_ORDER_READY,STATUS_ORDER_READY);
        pedidoEntity.setIdStatus(pedidoStatus);

        // Stubbing
        when(pedidoPersistencePort.findPedidoById(idPedido)).thenReturn(pedidoEntity);

        // Act & Assert
        assertThrows(CantMarkOrderReadyException.class, () -> {
            pedidoUseCase.pedidoListo(idPedido, authorization);
        });

        // Verify that pedidoPersistencePort.pedidoListo() was not called
        verify(pedidoPersistencePort, never()).pedidoListo(any());
    }

    @Test
    void pedidoListo_InvalidAuthorization_ThrowsUserCantMarkOrderReadyException() {
        // Arrange
        Long idPedido = 1L;
        Long authorization = 2L;
        PedidoEntity pedidoEntity = new PedidoEntity();
        PedidoStatusEntity pedidoStatus = new PedidoStatusEntity(STATUS_ORDER_IN_PREPARATION_ID,STATUS_ORDER_IN_PREPARATION,STATUS_ORDER_IN_PREPARATION);
        pedidoEntity.setIdStatus(pedidoStatus);
        EmpleadoRestauranteEntity empleadoRestaurante = new EmpleadoRestauranteEntity();
        empleadoRestaurante.setIdEmpleado(3L);
        pedidoEntity.setIdChef(empleadoRestaurante);

        // Stubbing
        when(pedidoPersistencePort.findPedidoById(idPedido)).thenReturn(pedidoEntity);

        // Act & Assert
        assertThrows(UserCantMarkOrderReadyException.class, () -> {
            pedidoUseCase.pedidoListo(idPedido, authorization);
        });

        // Verify that pedidoPersistencePort.pedidoListo() was not called
        verify(pedidoPersistencePort, never()).pedidoListo(any());
    }
    @Test
    public void finalizarPedido_ValidPinPedido_PedidoFinalizadoCorrectamente() {
        // Arrange
        Long idPedido = 1L;
        Long authorization = 123L;
        String pinPedido = "1234";
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, -30);
        Date fecha = calendar.getTime();
        PedidoEntity pedido = new PedidoEntity();
        EmpleadoRestauranteEntity empleadoRestaurante = new EmpleadoRestauranteEntity();
        empleadoRestaurante.setIdEmpleado(authorization);
        pedido.setId(idPedido);
        pedido.setIdChef(empleadoRestaurante);
        pedido.setFecha(fecha);
        RestauranteEntity restaurante = new RestauranteEntity();
        restaurante.setId(1L);
        pedido.setIdRestaurante(restaurante);
        PedidoStatusEntity pedidoStatus = new PedidoStatusEntity(STATUS_ORDER_IN_READY_ID,STATUS_ORDER_READY,STATUS_ORDER_READY);
        pedido.setIdStatus(pedidoStatus);
        pedido.setPinPedido(pinPedido);

        FinalizarPedidoReqDto finalizarPedidoReqDto = new FinalizarPedidoReqDto(idPedido,pinPedido);

        when(pedidoPersistencePort.findPedidoById(idPedido)).thenReturn(pedido);

        // Act
        pedidoUseCase.finalizarPedido(finalizarPedidoReqDto, authorization);

        // Assert
        verify(pedidoPersistencePort, times(1)).finalizarPedido(pedido);

        // Capturar los argumentos del método sendNotificationToUser
        ArgumentCaptor<String> mensajeCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<String> phoneNumberCaptor = ArgumentCaptor.forClass(String.class);
        verify(messengerClientService, times(1)).sendMessage(mensajeCaptor.capture(), phoneNumberCaptor.capture());

        assertEquals("Order #1 finished correctly", mensajeCaptor.getValue());
        assertEquals("+573188227631", phoneNumberCaptor.getValue());
    }
    @Test
    public void finalizarPedido_UsuarioNoEsChef_LanzaUserCantFinishedOrderException() {
        // Arrange
        Long idPedido = 1L;
        Long authorization = 123L;
        Long otherUser = 456L;
        String pinPedido = "1234";
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, -30);
        Date fecha = calendar.getTime();
        PedidoEntity pedido = new PedidoEntity();
        pedido.setId(idPedido);
        EmpleadoRestauranteEntity empleadoRestaurante = new EmpleadoRestauranteEntity();
        empleadoRestaurante.setIdEmpleado(otherUser);
        pedido.setIdChef(empleadoRestaurante);
        pedido.setFecha(fecha);
        PedidoStatusEntity pedidoStatus = new PedidoStatusEntity(STATUS_ORDER_IN_READY_ID,STATUS_ORDER_READY,STATUS_ORDER_READY);
        pedido.setIdStatus(pedidoStatus);
        pedido.setPinPedido(pinPedido);

        FinalizarPedidoReqDto finalizarPedidoReqDto = new FinalizarPedidoReqDto(idPedido,pinPedido);

        when(pedidoPersistencePort.findPedidoById(idPedido)).thenReturn(pedido);

        // Act & Assert
        assertThrows(UserCantFinishedOrderException.class, () -> {
            pedidoUseCase.finalizarPedido(finalizarPedidoReqDto, authorization);
        });

        verify(pedidoPersistencePort, never()).finalizarPedido(pedido);
        verify(messengerClientService, never()).sendMessage(anyString(), anyString());
    }
    @Test
    public void finalizarPedido_PedidoNoEnEstadoReady_LanzaCantMarkOrderFinishedException() {
        // Arrange
        Long idPedido = 1L;
        Long authorization = 123L;
        String pinPedido = "1234";
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, -30);
        Date fecha = calendar.getTime();
        PedidoEntity pedido = new PedidoEntity();
        pedido.setId(idPedido);
        pedido.setFecha(fecha);
        PedidoStatusEntity pedidoStatus = new PedidoStatusEntity(STATUS_ORDER_IN_PENDING_ID,STATUS_ORDER_PENDING,STATUS_ORDER_PENDING);
        pedido.setIdStatus(pedidoStatus);
        pedido.setPinPedido(pinPedido);

        FinalizarPedidoReqDto finalizarPedidoReqDto = new FinalizarPedidoReqDto(idPedido,pinPedido);

        when(pedidoPersistencePort.findPedidoById(idPedido)).thenReturn(pedido);

        // Act & Assert
        assertThrows(CantMarkOrderFinishedException.class, () -> {
            pedidoUseCase.finalizarPedido(finalizarPedidoReqDto, authorization);
        });

        verify(pedidoPersistencePort, never()).finalizarPedido(pedido);
        verify(messengerClientService, never()).sendMessage(anyString(), anyString());
    }
    @Test
    public void finalizarPedido_InvalidPinPedido_LanzaPinWrongException() {
        // Arrange
        Long idPedido = 1L;
        Long authorization = 123L;
        String pinPedido = "1234";
        String invalidPinPedido = "5678";

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, -30);
        Date fecha = calendar.getTime();
        PedidoEntity pedido = new PedidoEntity();
        pedido.setId(idPedido);
        EmpleadoRestauranteEntity empleadoRestaurante = new EmpleadoRestauranteEntity();
        empleadoRestaurante.setIdEmpleado(authorization);
        pedido.setIdChef(empleadoRestaurante);
        pedido.setFecha(fecha);
        PedidoStatusEntity pedidoStatus = new PedidoStatusEntity(STATUS_ORDER_IN_READY_ID,STATUS_ORDER_READY,STATUS_ORDER_READY);
        pedido.setIdStatus(pedidoStatus);
        pedido.setPinPedido(invalidPinPedido);
        FinalizarPedidoReqDto finalizarPedidoReqDto = new FinalizarPedidoReqDto(idPedido,pinPedido);
        when(pedidoPersistencePort.findPedidoById(idPedido)).thenReturn(pedido);

        // Act & Assert
        assertThrows(PinWrongException.class, () -> {
            pedidoUseCase.finalizarPedido(finalizarPedidoReqDto, authorization);
        });

        verify(pedidoPersistencePort, never()).finalizarPedido(pedido);
        verify(messengerClientService, never()).sendMessage(anyString(), anyString());
    }
    @Test
    void cancelarPedido_ValidIdPedidoAndAuthorization_CancelsPedido() {
        // Arrange
        Long idPedido = 1L;
        Long authorization = 123L;
        Long idCliente = 123L;
        PedidoEntity pedido = new PedidoEntity();
        pedido.setId(idPedido);
        EmpleadoRestauranteEntity empleadoRestaurante = new EmpleadoRestauranteEntity();
        empleadoRestaurante.setIdEmpleado(authorization);
        pedido.setIdChef(empleadoRestaurante);
        PedidoStatusEntity pedidoStatus = new PedidoStatusEntity(STATUS_ORDER_IN_PENDING_ID,STATUS_ORDER_PENDING,STATUS_ORDER_PENDING);
        pedido.setIdStatus(pedidoStatus);
        pedido.setIdCliente(idCliente);

        when(pedidoPersistencePort.findPedidoById(idPedido)).thenReturn(pedido);

        // Act
        pedidoUseCase.cancelarPedido(idPedido, authorization);

        // Assert
        verify(pedidoPersistencePort, times(1)).findPedidoById(idPedido);
        verify(pedidoPersistencePort, times(1)).cancelarPedido(pedido);
        verify(messengerClientService, times(1)).sendMessage(anyString(), anyString());
    }
    @Test
    void cancelarPedido_InvalidIdPedido_ThrowsParametersNegativesException() {
        // Arrange
        Long idPedido = -1L;
        Long authorization = 123L;

        // Act & Assert
        assertThrows(ParametersNegativesException.class, () -> {
            pedidoUseCase.cancelarPedido(idPedido, authorization);
        });

        verify(pedidoPersistencePort, never()).findPedidoById(idPedido);
        verify(pedidoPersistencePort, never()).cancelarPedido(any());
        verify(messengerClientService, never()).sendMessage(anyString(), anyString());
    }

    @Test
    void cancelarPedido_UnauthorizedUser_ThrowsUserItsNotOwnerOrderException() {
        // Arrange
        Long idPedido = 1L;
        Long authorization = 123L;
        PedidoEntity pedido = new PedidoEntity();
        pedido.setId(idPedido);
        PedidoStatusEntity pedidoStatus = new PedidoStatusEntity(STATUS_ORDER_IN_PENDING_ID,STATUS_ORDER_PENDING,STATUS_ORDER_PENDING);
        pedido.setIdStatus(pedidoStatus);
        pedido.setIdCliente(456L);

        when(pedidoPersistencePort.findPedidoById(idPedido)).thenReturn(pedido);

        // Act & Assert
        assertThrows(UserItsNotOwnerOrderException.class, () -> {
            pedidoUseCase.cancelarPedido(idPedido, authorization);
        });

        verify(pedidoPersistencePort, times(1)).findPedidoById(idPedido);
        verify(pedidoPersistencePort, never()).cancelarPedido(any());
        verify(messengerClientService, never()).sendMessage(anyString(), anyString());
    }

    @Test
    void cancelarPedido_OrderNotInPendingStatus_ThrowsUserCantCancelOrderException() {
        // Arrange
        Long idPedido = 1L;
        Long authorization = 123L;
        PedidoEntity pedido = new PedidoEntity();
        pedido.setId(idPedido);
        pedido.setIdCliente(authorization);
        PedidoStatusEntity pedidoStatus = new PedidoStatusEntity(STATUS_ORDER_IN_READY_ID,STATUS_ORDER_READY,STATUS_ORDER_READY);
        pedido.setIdStatus(pedidoStatus);

        when(pedidoPersistencePort.findPedidoById(idPedido)).thenReturn(pedido);

        // Act & Assert
        assertThrows(UserCantCancelOrderException.class, () -> {
            pedidoUseCase.cancelarPedido(idPedido, authorization);
        });

        verify(pedidoPersistencePort, times(1)).findPedidoById(idPedido);
        verify(pedidoPersistencePort, never()).cancelarPedido(any());
        verify(messengerClientService, never()).sendMessage(anyString(), anyString());
    }
    @Test
    void testGetTiempoPedidos() {
        // Arrange
        int pageNumber = 0;
        int pageSize = 10;
        Long idRestaurante = 1L;
        String campoOrdenar = "fecha";
        String orden = "asc";
        Long authorization = 123L;
        Pageable pageable = Pageable.ofSize(pageSize).withPage(pageNumber);
        List<PedidoEntity> pedidoEntities = Arrays.asList(
                new PedidoEntity(),
                new PedidoEntity()
        );
        List<Pedido> pedidos = Arrays.asList(
                new Pedido(),
                new Pedido()
        );
        Page<PedidoEntity> expectedPage = new PageImpl<>(pedidoEntities, pageable, pedidoEntities.size());
        Page<Pedido> expectedPedidoPage = new PageImpl<>(pedidos, pageable, pedidos.size());

        when(restaurantePersistencePort.existsByIdAndIdPropietario(idRestaurante, authorization)).thenReturn(true);
        when(pedidoPersistencePort.getTiempoPedidos(pageNumber, pageSize, idRestaurante, campoOrdenar, orden)).thenReturn(expectedPedidoPage);

        // Act
        Page<Pedido> result = pedidoUseCase.getTiempoPedidos(pageNumber, pageSize, idRestaurante, campoOrdenar, orden, authorization);

        // Assert
        Assertions.assertEquals(expectedPage.getSize(), result.getSize());
        Assertions.assertEquals(expectedPage.getNumber(), result.getNumber());
        Assertions.assertEquals(expectedPage.getTotalElements(), result.getTotalElements());
        verify(restaurantePersistencePort).existsByIdAndIdPropietario(idRestaurante, authorization);
        verify(pedidoPersistencePort).getTiempoPedidos(pageNumber, pageSize, idRestaurante, campoOrdenar, orden);
    }
    @Test
    public void testGetRankingEmployees() {
        int numeroPagina = 0;
        int size = 10;
        Long idRestaurante = 1L;
        Long authorization = 123L;

        List<Object[]> mockedResults = new ArrayList<>();
        mockedResults.add(new Object[]{1L, 10.5});
        mockedResults.add(new Object[]{2L, 8.2});
        Page<Object[]> mockedPage = new PageImpl<>(mockedResults);

        Mockito.when(pedidoPersistencePort.obtenerRankingEmployeesPorTiempoPromedio(numeroPagina, size, idRestaurante)).thenReturn(mockedPage);
        when(restaurantePersistencePort.existsByIdAndIdPropietario(idRestaurante,authorization)).thenReturn(true);

        Page<RankingEmployeesResDto> resultPage = pedidoUseCase.getRankingEmployees(numeroPagina, size, idRestaurante, authorization);

        Assertions.assertEquals(mockedPage.getTotalElements(), resultPage.getTotalElements());
        Assertions.assertEquals(mockedPage.getTotalPages(), resultPage.getTotalPages());

        Mockito.verify(pedidoPersistencePort).obtenerRankingEmployeesPorTiempoPromedio(numeroPagina, size, idRestaurante);
    }
}
