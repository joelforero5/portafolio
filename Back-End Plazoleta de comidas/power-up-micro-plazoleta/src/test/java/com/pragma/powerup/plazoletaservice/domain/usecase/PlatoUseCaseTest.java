package com.pragma.powerup.plazoletaservice.domain.usecase;

import com.pragma.powerup.plazoletaservice.domain.spi.IPlatoPersistencePort;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static com.pragma.powerup.plazoletaservice.utils.PlatoModeloValues.INSTANCE_DISH;
import static org.mockito.Mockito.verify;

class PlatoUseCaseTest {
    @Mock
    private IPlatoPersistencePort platoPersistencePort;
    @Mock
    PlatoUseCase platoUseCase;
    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
        platoUseCase = new PlatoUseCase(platoPersistencePort);
    }

    @Test
    void savePlato() {
        platoUseCase.savePlato(INSTANCE_DISH,1L);
        verify(platoPersistencePort).savePlato(INSTANCE_DISH,1L);
    }

    @Test
    public void testUpdateDish(){
        platoUseCase.updatePlato(INSTANCE_DISH,1L);
        verify(platoPersistencePort).updatePlato(INSTANCE_DISH,1L);
    }

    @Test
    public void testUpdateDishStatus(){
        platoUseCase.updatePlatoActivo(INSTANCE_DISH,1L);
        verify(platoPersistencePort).updatePlatoActivo(INSTANCE_DISH,1L);
    }

    @Test
    void getAllPlatos() {
        int numeroPagina = 0;
        int size = 10;
        Long idCategoriaOrdenar = 1L;
        Long idRestaurante = 1L;
        String campoOrdenar = "nombre";
        String orden = "ASC";
        platoUseCase.getAllPlatos(numeroPagina, size, idCategoriaOrdenar, idRestaurante, campoOrdenar, orden);
        verify(platoPersistencePort).getAllPlatos(numeroPagina, size, idCategoriaOrdenar, idRestaurante, campoOrdenar, orden);
    }

}
