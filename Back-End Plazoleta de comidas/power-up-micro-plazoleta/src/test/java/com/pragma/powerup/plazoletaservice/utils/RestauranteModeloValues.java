package com.pragma.powerup.plazoletaservice.utils;

import com.pragma.powerup.plazoletaservice.domain.model.Restaurante;

public class RestauranteModeloValues {
    public static final Restaurante INSTANCE_RESTAURANT = new Restaurante(
            7L,
            "Mote de queso",
            "Los alpes MZ Z L 8",
            3L,
            "+573004709632",
            "https://www.uhdpaper.com/2023/04/night-sky-clouds-sunset-scenery-4k-7700i.html",
            "100000"
    );

}