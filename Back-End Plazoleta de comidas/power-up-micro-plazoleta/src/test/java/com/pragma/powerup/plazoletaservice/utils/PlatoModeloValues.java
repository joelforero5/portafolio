package com.pragma.powerup.plazoletaservice.utils;

import com.pragma.powerup.plazoletaservice.domain.model.Categoria;
import com.pragma.powerup.plazoletaservice.domain.model.PedidoPlato;
import com.pragma.powerup.plazoletaservice.domain.model.Plato;
import static com.pragma.powerup.plazoletaservice.utils.RestauranteModeloValues.INSTANCE_RESTAURANT;
public class PlatoModeloValues {

    private static final Categoria category = new Categoria(2L,"bebidas","liquidos");
    public static final Plato INSTANCE_DISH = new Plato(
            "cd",
            category,
            "Mote de queso",
            100000L,
            INSTANCE_RESTAURANT,
            "https://youtu.be/m1a_GqJf02M",
            true
    );
}