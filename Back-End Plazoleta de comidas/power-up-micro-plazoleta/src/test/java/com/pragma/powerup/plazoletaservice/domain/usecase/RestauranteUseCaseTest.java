package com.pragma.powerup.plazoletaservice.domain.usecase;

import com.pragma.powerup.plazoletaservice.adapters.driven.jpa.mysql.exceptions.UserNotFoundException;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.client.service.IUserClientService;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.dto.request.EmployeeRequestDto;
import com.pragma.powerup.plazoletaservice.adapters.driving.http.mapper.IEmpleadoRestauranteReqMapper;
import com.pragma.powerup.plazoletaservice.domain.model.EmpleadoRestaurante;
import com.pragma.powerup.plazoletaservice.domain.model.Restaurante;
import com.pragma.powerup.plazoletaservice.domain.spi.IRestaurantePersistencePort;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class RestauranteUseCaseTest {

    private RestauranteUseCase restauranteUseCase;

    @Mock
    private IRestaurantePersistencePort restaurantePersistencePort;

    @Mock
    private IUserClientService userClientService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        restauranteUseCase = new RestauranteUseCase(restaurantePersistencePort, userClientService);
    }

    @Test
    void saveRestaurante_UserExists_Success() {
        Restaurante restaurante=createRestaurante();
        // Arrange
        when(userClientService.existsOwner(restaurante.getIdPropietario())).thenReturn(true);
        boolean exists=userClientService.existsOwner(restaurante.getIdPropietario());
        if (exists){
            restaurantePersistencePort.saveRestaurante(restaurante);
        }

        // Assert
        verify(userClientService, times(1)).existsOwner(restaurante.getIdPropietario());
        verify(restaurantePersistencePort, times(1)).saveRestaurante(restaurante);
    }

    @Test
    void saveRestaurante_UserNotExists_UserNotFoundExceptionThrown() {
        // Arrange
        Restaurante restaurante = createRestaurante();
        when(userClientService.existsOwner(restaurante.getIdPropietario())).thenReturn(false);

        // Act
        assertThrows(UserNotFoundException.class, () -> restauranteUseCase.saveRestaurante(restaurante));

        // Assert
        verify(userClientService, times(1)).existsOwner(restaurante.getIdPropietario());
        verify(restaurantePersistencePort, never()).saveRestaurante(restaurante);
    }
    @Test
    void testGetAllRestaurantes() {
        int pageNumber = 0;
        int pageSize = 10;
        restauranteUseCase.getAllRestaurantes(pageNumber, pageSize);
        verify(restaurantePersistencePort).getAllRestaurantesPage(pageNumber, pageSize);
    }

    private Restaurante createRestaurante(){
        return new Restaurante(2L,"Jarris","Local 1",12L,
                "+573201564898","www.logo.com","1234");
    }

    @Test
    void saveEmployee_WithValidData_ShouldSaveEmployee() {
        // Arrange
        Long idEmpleado = 1L;
        Long idRestaurante = 2L;
        Long idPropietario = 3L;

        EmployeeRequestDto employeeRequestDto = new EmployeeRequestDto(idEmpleado,idRestaurante);


        Restaurante restaurante = new Restaurante(employeeRequestDto.getIdRestaurante(),null,null,null,null,null,null);
        restaurante.setIdPropietario(idPropietario);

        when(userClientService.existsOwner(anyLong())).thenReturn(true);
        when(restaurantePersistencePort.existsByIdAndIdPropietario(anyLong(), anyLong())).thenReturn(true);
        when(userClientService.existsEmployee(anyLong())).thenReturn(true);
        when(restaurantePersistencePort.existsEmployeeByIdAndIdRestaurante(anyLong(), any(Restaurante.class))).thenReturn(false);

        // Act & Assert
        assertDoesNotThrow(() -> restauranteUseCase.saveEmployee(employeeRequestDto, idPropietario));

        // Assert
        verify(restaurantePersistencePort, times(1)).saveEmployee(any(EmpleadoRestaurante.class));
    }
}