package com.pragma.powerup.usermicroservice.domain.validation;

import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import java.util.Calendar;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class UserValidationTest {

    @Test
    public void testIsAdult() {
        // Caso de prueba: Fecha de nacimiento de una persona adulta (mayor o igual a 18 años)
        Calendar cal = Calendar.getInstance();
        cal.set(1999, Calendar.JULY, 12);
        Date fechaNacimientoAdulto = cal.getTime();
        boolean esAdulto = UserValidation.isAdult(fechaNacimientoAdulto);
        Assertions.assertTrue(esAdulto);


        cal.set(2008, Calendar.JULY, 12);
        Date fechaNacimientoMenor = cal.getTime(); // Por ejemplo, 15 de junio de 2005
        boolean esMenor = UserValidation.isAdult(fechaNacimientoMenor);
        Assert.assertFalse(esMenor);
    }
}