package com.pragma.powerup.usermicroservice.domain.usecase;

import com.pragma.powerup.usermicroservice.configuration.Constants;
import com.pragma.powerup.usermicroservice.domain.exceptions.*;
import com.pragma.powerup.usermicroservice.domain.model.Role;
import com.pragma.powerup.usermicroservice.domain.model.User;
import com.pragma.powerup.usermicroservice.domain.spi.IUserPersistencePort;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import java.util.Calendar;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest
class UserUseCaseTest {
    @Mock
    private IUserPersistencePort userPersistencePort;
    @InjectMocks
    private UserUseCase userUseCase;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        userUseCase = new UserUseCase(userPersistencePort);
    }

    @Test
    void testSaveOwner_ValidUser_Successful() {
        // Arrange
        User user = generateUser(1999);
        when(userPersistencePort.existsByNumeroDocumento(user.getNumeroDocumento())).thenReturn(false);
        when(userPersistencePort.existsByCorreo(user.getCorreo())).thenReturn(false);
        when(userPersistencePort.existsByCelular(user.getCelular())).thenReturn(false);

        assertDoesNotThrow(()->{
            userUseCase.saveOwner(user);
        });
        //verify(userPersistencePort, times(1)).saveOwner(user);
    }
    @Test
    void testSaveEmployee() {
        // Arrange
        User user = generateUser(1999);
        user.setRole(new Role(Constants.EMPLOYEE_ROLE_ID,Constants.ROLE_EMPLOYEE,Constants.ROLE_EMPLOYEE));
        when(userPersistencePort.existsByNumeroDocumento(user.getNumeroDocumento())).thenReturn(false);
        when(userPersistencePort.existsByCorreo(user.getCorreo())).thenReturn(false);
        when(userPersistencePort.existsByCelular(user.getCelular())).thenReturn(false);

        assertDoesNotThrow(()->{
            userUseCase.saveEmployee(user);
        });
        //verify(userPersistencePort, times(1)).saveOwner(user);
    }
    @Test
    void testSaveClient() {
        // Arrange
        User user = generateUser(1999);
        user.setRole(new Role(Constants.CLIENT_ROLE_ID,Constants.ROLE_CLIENT,Constants.ROLE_CLIENT));
        when(userPersistencePort.existsByNumeroDocumento(user.getNumeroDocumento())).thenReturn(false);
        when(userPersistencePort.existsByCorreo(user.getCorreo())).thenReturn(false);
        when(userPersistencePort.existsByCelular(user.getCelular())).thenReturn(false);

        assertDoesNotThrow(()->{
            userUseCase.saveClient(user);
        });
        //verify(userPersistencePort, times(1)).saveOwner(user);
    }

    @Test
    void testSaveOwner_UnderageUser_ThrowsUserIsNotAdultException() {
        User user = generateUser(2020);
        assertThrows(UserIsNotAdultException.class,()->{
            userUseCase.saveOwner(user);
        });

    }

    @Test
    void testSaveOwner_DuplicateNumeroDocumento_ThrowsUserAlreadyExistsException() {
        // Arrange
        User user = generateUser(1999);

        when(userPersistencePort.existsByNumeroDocumento(user.getNumeroDocumento())).thenReturn(true);
        // Act
        assertThrows(UserAlreadyExistsException.class,()->{
            userUseCase.saveOwner(user);
        });
        verify(userPersistencePort, times(1)).existsByNumeroDocumento(user.getNumeroDocumento());

        // Assert
        // The expected exception is defined in the @Test annotation
    }

    @Test
    void testSaveOwner_DuplicateMail_ThrowsMailAlreadyExistException() {
        // Arrange
        User user = generateUser(1999);
        // Set up any necessary properties for the user object

        // Mock the behavior of the userPersistencePort
        when(userPersistencePort.existsByCorreo(user.getCorreo())).thenReturn(true);

        // Act
        assertThrows(MailAlreadyExistsException.class,()->{
            userUseCase.saveOwner(user);
        });
        verify(userPersistencePort, times(1)).existsByCorreo(user.getCorreo());

        // Assert
        // The expected exception is defined in the @Test annotation
    }
    @Test
    void testSaveOwner_DuplicateCellPhone_ThrowsCellPhoneAlreadyExistException() {
        User user = generateUser(1999);
        when(userPersistencePort.existsByCelular(user.getCelular())).thenReturn(true);

        // Act
        assertThrows(CellphoneAlreadyExistException.class,()->{
            userUseCase.saveOwner(user);
        });
        verify(userPersistencePort, times(1)).existsByCelular(user.getCelular());

        // Assert
        // The expected exception is defined in the @Test annotation
    }
    @Test
    void testSaveOwner_Null_BirthDay() {
        User user = generateUser(1999);
        user.setFechaNacimiento(null);
        // Act
        assertThrows(BirthDayDateNotCouldBeNullException.class,()->{
            userUseCase.saveOwner(user);
        });
        // Assert
        // The expected exception is defined in the @Test annotation
    }


    @Test
    void getOwner() {
        Long id = 2L;
        User expectedUser = generateUser(1999);
        when(userPersistencePort.getOwner(id)).thenReturn(expectedUser);
        User userActual = userUseCase.getOwner(id);
        assertEquals(expectedUser, userActual);
        verify(userPersistencePort,times(1)).getOwner(id);
        
    }

    private User generateUser(int anoNacimiento){
        User user = new User();
        Calendar cal = Calendar.getInstance();
        cal.set(anoNacimiento, Calendar.JULY, 12);
        Date fechaNacimiento = cal.getTime();
        Role role = new Role(Constants.OWNER_ROLE_ID,Constants.ROLE_OWNER,Constants.ROLE_OWNER);
        user.setId(role.getId());
        user.setNombre("Joel");
        user.setApellido("Forero");
        user.setNumeroDocumento("1095841559");
        user.setCelular("+573188227631");
        user.setFechaNacimiento(fechaNacimiento);
        user.setCorreo("pruebajoel@gmail.com");
        user.setClave("1234");
        user.setRole(role);
        return user;
    }
    // Add more test methods to cover other scenarios and exceptions
}

