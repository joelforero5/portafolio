package com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.adapter;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.RoleEntity;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.UserEntity;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.exceptions.UserNotFoundException;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.exceptions.UserNotSavedException;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.mappers.IUserEntityMapper;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.repositories.IUserRepository;
import com.pragma.powerup.usermicroservice.configuration.Constants;
import com.pragma.powerup.usermicroservice.domain.model.Role;
import com.pragma.powerup.usermicroservice.domain.model.User;
import org.junit.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Calendar;
import java.util.Date;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

class UserMysqlAdapterTest {
    @Mock
    private  IUserRepository userRepository;
    @Mock
    private  IUserEntityMapper userEntityMapper;
    @Mock
    private  PasswordEncoder passwordEncoder;
    private RoleEntity roleEntity;
    @InjectMocks
    UserMysqlAdapter userMysqlAdapter;
    @BeforeEach
    void setUp(){
        MockitoAnnotations.openMocks(this);
        roleEntity = new RoleEntity();
        roleEntity.setId(Constants.OWNER_ROLE_ID);
        roleEntity.setNombre(Constants.ROLE_OWNER);
        roleEntity.setDescripcion(Constants.ROLE_OWNER);
        userMysqlAdapter = new UserMysqlAdapter(userRepository, userEntityMapper, passwordEncoder);

    }
    @Test
    void testSaveOwner_Successful() {
        User user = generateUser();
        UserEntity userEntity = generateUserEntity();
        when(userEntityMapper.toEntity(user)).thenReturn(userEntity);
        when(passwordEncoder.encode("1234")).thenReturn("encodedPassword");

        // Act
        userMysqlAdapter.saveOwner(user);

        // Assert
        verify(passwordEncoder, times(1)).encode("1234");
        verify(userRepository, times(1)).save(userEntity);
    }
    @Test
    void testSaveOwner_Exception() {
        // Arrange
        User user = generateUser();
        UserEntity userEntity = generateUserEntity();
        when(userEntityMapper.toEntity(user)).thenReturn(userEntity);
        when(userRepository.save(userEntityMapper.toEntity(user))).thenThrow(new RuntimeException());

        // Act and Assert
        assertThrows(UserNotSavedException.class, () -> userMysqlAdapter.saveOwner(user));
    }

    @Test
    void testGetOwner_UserFound() {
        // Arrange
        Long id = 2L;
        UserEntity userEntity = generateUserEntity();
        User user = generateUser();
        // Arrange

        when(userRepository.findByIdAndRole(eq(id), eq(roleEntity))).thenReturn(Optional.of(userEntity));
        when(userEntityMapper.toUser(userEntity)).thenReturn(user);
        Optional<UserEntity> optionalUserEntity = userRepository.findByIdAndRole(id,roleEntity);
        User userActual = userEntityMapper.toUser(optionalUserEntity.get());
        assertEquals(user,userActual);
        verify(userRepository, times(1)).findByIdAndRole(eq(id), eq(roleEntity));
        verify(userEntityMapper, times(1)).toUser(userEntity);
    }

    @Test
    void testGetOwner_UserNotFound() {
        Long id = 2L;
        UserEntity userEntity = generateUserEntity();
        User user = generateUser();
        // Arrange

        when(userRepository.findByIdAndRole(eq(id), eq(roleEntity))).thenReturn(Optional.empty());
        when(userEntityMapper.toUser(userEntity)).thenReturn(user);
        Optional<UserEntity> optionalUserEntity = userRepository.findByIdAndRole(id,roleEntity);
        assertTrue(optionalUserEntity.isEmpty());
        assertThrows(UserNotFoundException.class,()->userMysqlAdapter.getOwner(id));
        verify(userRepository, times(1)).findByIdAndRole(eq(id), eq(roleEntity));

    }
    @Test
    void existOwner() {
        Long id = 2L;
        when(userRepository.existsByIdAndRole(id,roleEntity)).thenReturn(true,false);
        boolean exist = userRepository.existsByIdAndRole(id,roleEntity);
        assertTrue(exist);
        boolean notExist = userRepository.existsByIdAndRole(id,roleEntity);
        assertFalse(notExist);
        verify(userRepository,times(2)).existsByIdAndRole(id,roleEntity);
    }
    private UserEntity generateUserEntity(){
        UserEntity userEntity = new UserEntity();
        Calendar cal = Calendar.getInstance();
        cal.set(1999, Calendar.JULY, 12);
        Date fechaNacimiento = cal.getTime();
        RoleEntity role = new RoleEntity(Constants.OWNER_ROLE_ID,Constants.ROLE_OWNER,Constants.ROLE_OWNER);
        userEntity.setId(role.getId());
        userEntity.setNombre("Joel");
        userEntity.setApellido("Forero");
        userEntity.setNumeroDocumento("1095841559");
        userEntity.setCelular("+573188227631");
        userEntity.setFechaNacimiento(fechaNacimiento);
        userEntity.setCorreo("pruebajoel@gmail.com");
        userEntity.setClave("1234");
        userEntity.setRole(role);
        return userEntity;
    }
    private User generateUser(){
        User user = new User();
        Calendar cal = Calendar.getInstance();
        cal.set(1999, Calendar.JULY, 12);
        Date fechaNacimiento = cal.getTime();
        Role role = new Role(Constants.OWNER_ROLE_ID,Constants.ROLE_OWNER,Constants.ROLE_OWNER);
        user.setId(role.getId());
        user.setNombre("Joel");
        user.setApellido("Forero");
        user.setNumeroDocumento("1095841559");
        user.setCelular("+573188227631");
        user.setFechaNacimiento(fechaNacimiento);
        user.setCorreo("pruebajoel@gmail.com");
        user.setClave("1234");
        user.setRole(role);
        return user;
    }


}