INSERT INTO User (id, nombre, apellido, numero_documento, celular, fecha_nacimiento, correo, clave, role_id)
VALUES (1, 'Jaime', 'Jaimes', '123', '555-1234', '1990-01-01', 'juan.perez@example.com', '$2a$10$GlsGSNhkbVon6ZOSNMptOu5RikedRzlCAhMa7YpwvUSS0c88WT99S', 1);


INSERT INTO role (`id`, `descripcion`, `nombre`) VALUES ('1', 'ROLE_ADMIN', 'ROLE_ADMIN');
INSERT INTO role (`id`, `descripcion`, `nombre`) VALUES ('2', 'ROLE_OWNER', 'ROLE_OWNER');
INSERT INTO role (`id`, `descripcion`, `nombre`) VALUES ('3', 'ROLE_EMPLOYEE', 'ROLE_EMPLOYEE');
INSERT INTO role (`id`, `descripcion`, `nombre`) VALUES ('4', 'ROLE_CLIENT', 'ROLE_CLIENT');