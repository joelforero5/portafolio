package com.pragma.powerup.usermicroservice.domain.api;

import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.UserResponseDto;
import com.pragma.powerup.usermicroservice.domain.model.User;

import java.util.List;

public interface IUserServicePort {
    void saveOwner(User user);
    void saveEmployee(User user);
    void saveClient(User user);
    void deleteUser(User user);
    List<User> getAllUsers(int page);
    User getUser(Long id);
    User getAdmin(Long id);
    User getOwner(Long id);
    Boolean existOwner(Long id);
    User getEmployee(Long id);
    User getClient(Long id);

    Boolean existClient(Long id);

    Boolean existsEmployee(Long id);
}
