package com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.*;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Date;

@AllArgsConstructor
@Getter
public class UserRequestDto {
    @Schema(title = "nombre", description = "Nombre", example = "Jose")
    @NotBlank
    private String nombre;
    @Schema(title = "apellido", description = "Apellido", example = "Jimenez")
    @NotBlank
    private String apellido;
    @Schema(title = "numeroDocumento", description = "Numero de Documento", example = "1098874553")
    @NotBlank
    @Pattern(regexp = "^[0-9]+$", message = "El numero de documento debe ser numérico")
    private String numeroDocumento;
    @Schema(title = "celular", description = "Celular", example = "+573188993645")
    @NotBlank
    @Pattern(regexp = "^\\+?\\d{1,12}$", message = "El número de celular debe tener un formato valido")
    private String celular;
    @Past
    private Date fechaNacimiento;
    @Schema(title = "correo", description = "Correo", example = "jose@gmail.com")
    @NotBlank
    @Email
    @Pattern(regexp = "^[\\w\\.-]+@[\\w\\.-]+\\.\\w{2,}(\\.\\w{2})?$",message = "La dirección de correo no tiene un formato valido")
    private String correo;
    @Schema(title = "clave", description = "Clave", example = "1234")
    @NotBlank
    private String clave;
}
