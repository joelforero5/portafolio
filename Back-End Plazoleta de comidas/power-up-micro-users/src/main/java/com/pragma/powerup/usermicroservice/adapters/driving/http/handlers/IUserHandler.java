package com.pragma.powerup.usermicroservice.adapters.driving.http.handlers;

import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.UserRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.UserResponseDto;
import com.pragma.powerup.usermicroservice.domain.model.User;

import java.util.List;

public interface IUserHandler {
    void saveOwner(UserRequestDto userRequestDto);
    void saveEmployee(UserRequestDto userRequestDto);
    void saveClient(UserRequestDto userRequestDto);
    void deleteUser(UserRequestDto userRequestDto);
    List<User> getAllUsers(int page);
    UserResponseDto getUser(Long id);
    User getAdmin(Long id);
    UserResponseDto getOwner(Long id);
    Boolean existOwner(Long id);
    Boolean existClient(Long id);
    Boolean existEmployee(Long id);
    User getClient(Long id);

}
