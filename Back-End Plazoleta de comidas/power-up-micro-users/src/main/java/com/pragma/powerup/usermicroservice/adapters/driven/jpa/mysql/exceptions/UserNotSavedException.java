package com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.exceptions;

public class UserNotSavedException extends RuntimeException{
    public UserNotSavedException(){super();}
}
