package com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.adapter;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.RoleEntity;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.UserEntity;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.exceptions.*;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.mappers.IUserEntityMapper;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.repositories.IRoleRepository;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.repositories.IUserRepository;
import com.pragma.powerup.usermicroservice.configuration.Constants;
import com.pragma.powerup.usermicroservice.domain.model.User;
import com.pragma.powerup.usermicroservice.domain.spi.IUserPersistencePort;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;


@RequiredArgsConstructor
@Transactional
@Getter
@Setter
public class UserMysqlAdapter implements IUserPersistencePort {
    private final IUserRepository userRepository;
    private final IUserEntityMapper userEntityMapper;
    private final PasswordEncoder passwordEncoder;

    @Override
    public Boolean existsByNumeroDocumento(String numeroDocumento) {
        return null;
    }

    @Override
    public Boolean existsByCelular(String celular) {
        return userRepository.existsByCelular(celular);
    }

    @Override
    public Boolean existsByCorreo(String correo) {
        return userRepository.existsByCorreo(correo);
    }

    @Override
    public Boolean existOwner(Long id) {
        RoleEntity roleEntity = new RoleEntity();
        roleEntity.setId(Constants.OWNER_ROLE_ID);
        roleEntity.setNombre(Constants.ROLE_OWNER);
        roleEntity.setDescripcion(Constants.ROLE_OWNER);
        return userRepository.existsByIdAndRole(id,roleEntity);
    }

    @Override
    public void saveOwner(User user) {
        try{
            user.setClave(passwordEncoder.encode(user.getClave()));
            userRepository.save(userEntityMapper.toEntity(user));
        }catch (Exception e){
            throw new UserNotSavedException();
        }
    }

    @Override
    public void saveEmployee(User user) {
        try{
            user.setClave(passwordEncoder.encode(user.getClave()));
            userRepository.save(userEntityMapper.toEntity(user));
        }catch (Exception e){
            throw new UserNotSavedException();
        }
    }

    @Override
    public void saveClient(User user) {
        try{
            user.setClave(passwordEncoder.encode(user.getClave()));
            userRepository.save(userEntityMapper.toEntity(user));
        }catch (Exception e){
            throw new UserNotSavedException();
        }
    }

    @Override
    public void deleteUser(User user) {

    }

    @Override
    public List<User> getAllUsers(int page) {
        return null;
    }

    @Override
    public User getUser(Long id) {
        return null;
    }

    @Override
    public User getAdmin(Long id) {
        return null;
    }

    @Override
    public User getOwner(Long id) {
        RoleEntity roleEntity = new RoleEntity();
        roleEntity.setId(Constants.OWNER_ROLE_ID);
        roleEntity.setNombre(Constants.ROLE_OWNER);
        roleEntity.setDescripcion(Constants.ROLE_OWNER);
        Optional<UserEntity> optionalUserEntity = userRepository.findByIdAndRole(id, roleEntity);
        if (optionalUserEntity.isEmpty()) {
            throw new UserNotFoundException();
        }
        UserEntity userEntity = userRepository.findByIdAndRole(id, roleEntity).orElseThrow(UserNotFoundException::new);
        return userEntityMapper.toUser(userEntity);
    }

    @Override
    public User getEmployee(Long id) {
        return null;
    }

    @Override
    public User getClient(Long id) {
        return null;
    }

    @Override
    public Boolean existClient(Long id) {
        RoleEntity roleEntity = new RoleEntity();
        roleEntity.setId(Constants.CLIENT_ROLE_ID);
        roleEntity.setNombre(Constants.ROLE_CLIENT);
        roleEntity.setDescripcion(Constants.ROLE_CLIENT);
        return userRepository.existsByIdAndRole(id,roleEntity);
    }

    @Override
    public Boolean existsEmployee(Long id) {
        RoleEntity roleEntity = new RoleEntity();
        roleEntity.setId(Constants.EMPLOYEE_ROLE_ID);
        roleEntity.setNombre(Constants.ROLE_EMPLOYEE);
        roleEntity.setDescripcion(Constants.ROLE_EMPLOYEE);
        return userRepository.existsByIdAndRole(id,roleEntity);
    }
}
