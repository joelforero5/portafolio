package com.pragma.powerup.usermicroservice.domain.validation;

import com.pragma.powerup.usermicroservice.domain.model.User;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Date;

public class UserValidation {
    private UserValidation() {
        throw new IllegalStateException("Utility class");
    }

    public static boolean isAdult(Date fechaNacimiento) {
        ZonedDateTime fechaNacimientoZonedDateTime = fechaNacimiento.toInstant().atZone(ZoneId.systemDefault());
        ZonedDateTime fechaActual = ZonedDateTime.now(ZoneId.systemDefault());
        long edad = ChronoUnit.YEARS.between(fechaNacimientoZonedDateTime, fechaActual);
        return edad >= 18;
    }

    public static void validateUser(User user){

    }
}
