package com.pragma.powerup.usermicroservice.domain.usecase;

import com.pragma.powerup.usermicroservice.configuration.Constants;
import com.pragma.powerup.usermicroservice.domain.api.IUserServicePort;
import com.pragma.powerup.usermicroservice.domain.exceptions.*;
import com.pragma.powerup.usermicroservice.domain.model.Role;
import com.pragma.powerup.usermicroservice.domain.model.User;
import com.pragma.powerup.usermicroservice.domain.spi.IUserPersistencePort;
import com.pragma.powerup.usermicroservice.domain.validation.UserValidation;
import org.springframework.http.server.reactive.HttpHandler;

import java.util.List;

public class UserUseCase implements IUserServicePort {
    private final IUserPersistencePort userPersistencePort;


    public UserUseCase(IUserPersistencePort userPersistencePort) {
        this.userPersistencePort = userPersistencePort;
    }


    @Override
    public void saveOwner(User user) {
        if (user.getFechaNacimiento() == null){throw new BirthDayDateNotCouldBeNullException();}
        if (!UserValidation.isAdult(user.getFechaNacimiento())){throw new UserIsNotAdultException();}
       if (Boolean.TRUE.equals(userPersistencePort.existsByNumeroDocumento(user.getNumeroDocumento()))){throw new UserAlreadyExistsException();}
       if (Boolean.TRUE.equals(userPersistencePort.existsByCorreo(user.getCorreo()))){throw new MailAlreadyExistsException();}
       if (Boolean.TRUE.equals(userPersistencePort.existsByCelular(user.getCelular()))){throw new CellphoneAlreadyExistException();}
        user.setRole(new Role(Constants.OWNER_ROLE_ID,Constants.ROLE_OWNER,Constants.ROLE_OWNER));
        userPersistencePort.saveOwner(user);
    }

    @Override
    public void saveEmployee(User user) {
        if (!UserValidation.isAdult(user.getFechaNacimiento())){throw new UserIsNotAdultException();}
        if (Boolean.TRUE.equals(userPersistencePort.existsByNumeroDocumento(user.getNumeroDocumento()))){throw new UserAlreadyExistsException();}
        if (Boolean.TRUE.equals(userPersistencePort.existsByCorreo(user.getCorreo()))){throw new MailAlreadyExistsException();}
        if (Boolean.TRUE.equals(userPersistencePort.existsByCelular(user.getCelular()))){throw new CellphoneAlreadyExistException();}
        user.setRole(new Role(Constants.EMPLOYEE_ROLE_ID,Constants.ROLE_EMPLOYEE,Constants.ROLE_EMPLOYEE));
        userPersistencePort.saveEmployee(user);
    }

    @Override
    public void saveClient(User user) {
        if (!UserValidation.isAdult(user.getFechaNacimiento())){throw new UserIsNotAdultException();}
        if (Boolean.TRUE.equals(userPersistencePort.existsByNumeroDocumento(user.getNumeroDocumento()))){throw new UserAlreadyExistsException();}
        if (Boolean.TRUE.equals(userPersistencePort.existsByCorreo(user.getCorreo()))){throw new MailAlreadyExistsException();}
        if (Boolean.TRUE.equals(userPersistencePort.existsByCelular(user.getCelular()))){throw new CellphoneAlreadyExistException();}
        user.setRole(new Role(Constants.CLIENT_ROLE_ID,Constants.ROLE_CLIENT,Constants.ROLE_CLIENT));
        userPersistencePort.saveClient(user);
    }

    @Override
    public void deleteUser(User user) {
        userPersistencePort.deleteUser(user);
    }

    @Override
    public List<User> getAllUsers(int page) {
        return userPersistencePort.getAllUsers(page);
    }

    @Override
    public User getUser(Long id) {

        return userPersistencePort.getUser(id);
    }

    @Override
    public User getAdmin(Long id) {
        return userPersistencePort.getAdmin(id);
    }

    @Override
    public User getOwner(Long id) {
        return userPersistencePort.getOwner(id);
    }

    @Override
    public Boolean existOwner(Long id) {
        return userPersistencePort.existOwner(id);
    }

    @Override
    public User getEmployee(Long id) {
        return userPersistencePort.getEmployee(id);
    }

    @Override
    public User getClient(Long id) {
        return userPersistencePort.getClient(id);
    }

    @Override
    public Boolean existClient(Long id) {
        return userPersistencePort.existClient(id);
    }

    @Override
    public Boolean existsEmployee(Long id) {
        return userPersistencePort.existsEmployee(id);
    }
}
