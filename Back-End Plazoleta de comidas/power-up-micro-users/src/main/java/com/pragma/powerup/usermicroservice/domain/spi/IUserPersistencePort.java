package com.pragma.powerup.usermicroservice.domain.spi;

import com.pragma.powerup.usermicroservice.domain.model.User;

import java.util.List;

public interface IUserPersistencePort {
    Boolean existsByNumeroDocumento(String numeroDocumento);
    Boolean existsByCelular(String celular);
    Boolean existsByCorreo(String correo);
    Boolean existOwner(Long id);
    void saveOwner(User user);
    void saveEmployee(User user);
    void saveClient(User user);
    void deleteUser(User user);
    List<User> getAllUsers(int page);
    User getUser(Long id);
    User getAdmin(Long id);
    User getOwner(Long id);
    User getEmployee(Long id);
    User getClient(Long id);

    Boolean existClient(Long id);

    Boolean existsEmployee(Long id);
}
