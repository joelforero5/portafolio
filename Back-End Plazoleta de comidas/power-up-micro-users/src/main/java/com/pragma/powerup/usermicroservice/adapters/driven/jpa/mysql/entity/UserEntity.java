package com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity;

import com.pragma.powerup.usermicroservice.domain.model.Role;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Entity
@Table(name = "user")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class UserEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false)
    private String nombre;
    @Column(nullable = false)
    private String apellido;
    @Column(name = "numero_documento",unique = true,nullable = false)
    private String numeroDocumento;
    @Column(unique = true,nullable = false)
    private String celular;
    @Column(name = "fecha_nacimiento",nullable = false)
    private Date fechaNacimiento;
    @Column(unique = true,nullable = false)
    private String correo;
    @Column(nullable = false)
    private String clave;
    @ManyToOne
    @JoinColumn(name="role_id")
    private RoleEntity role;
}
