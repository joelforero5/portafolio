package com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.repositories;

import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.RoleEntity;
import com.pragma.powerup.usermicroservice.adapters.driven.jpa.mysql.entity.UserEntity;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface IUserRepository extends JpaRepository<UserEntity, Long> {
    Optional<UserEntity> findByIdAndRole(Long idUser, RoleEntity role);
    Optional<UserEntity> findByNumeroDocumento(String numeroDocumento);
    Boolean existsByIdAndRole(Long id,RoleEntity role);
    Boolean existsByCelular(String celular);
    Boolean existsByCorreo(String correo);
    Optional<UserEntity> findByCelular(String celular);
    Optional<UserEntity> findByCorreo(String correo);
    List<UserEntity> findAllById(Long idUser);
    /*void deleteByPersonEntityIdAndRoleEntityId(Long idPerson, Long idRole);
    List<UserEntity> findAllByRoleEntityId(Long idRole, Pageable pageable);
    */

}
