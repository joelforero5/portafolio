package com.pragma.powerup.usermicroservice.domain.exceptions;

public class UserIsNotAdultException extends RuntimeException{
    public UserIsNotAdultException(){super();}
}
