package com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.impl;

import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.request.UserRequestDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.dto.response.UserResponseDto;
import com.pragma.powerup.usermicroservice.adapters.driving.http.handlers.IUserHandler;
import com.pragma.powerup.usermicroservice.adapters.driving.http.mapper.IUserRequestMapper;
import com.pragma.powerup.usermicroservice.adapters.driving.http.mapper.IUserResponseMapper;
import com.pragma.powerup.usermicroservice.domain.api.IRoleServicePort;
import com.pragma.powerup.usermicroservice.domain.api.IUserServicePort;
import com.pragma.powerup.usermicroservice.domain.model.User;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserHandlerImpl implements IUserHandler {

    private final IUserServicePort userServicePort;
    private final IRoleServicePort roleServicePort;
    private final IUserRequestMapper userRequestMapper;
    private final IUserResponseMapper userResponseMapper;


    @Override
    public void saveOwner(UserRequestDto userRequestDto) {
        userServicePort.saveOwner(userRequestMapper.toUser(userRequestDto));
    }

    @Override
    public void saveEmployee(UserRequestDto userRequestDto) {
        userServicePort.saveEmployee(userRequestMapper.toUser(userRequestDto));
    }

    @Override
    public void saveClient(UserRequestDto userRequestDto) {
        userServicePort.saveClient(userRequestMapper.toUser(userRequestDto));
    }

    @Override
    public void deleteUser(UserRequestDto userRequestDto) {
        userServicePort.deleteUser(userRequestMapper.toUser(userRequestDto));
    }

    @Override
    public List<User> getAllUsers(int page) {
        return null;
    }

    @Override
    public UserResponseDto getUser(Long id) {
        return null;
    }

    @Override
    public User getAdmin(Long id) {
        return null;
    }

    @Override
    public UserResponseDto getOwner(Long id) {
        return userResponseMapper.toResponse(userServicePort.getOwner(id));
    }

    @Override
    public Boolean existOwner(Long id) {
        return userServicePort.existOwner(id);
    }

    @Override
    public Boolean existClient(Long id) {
        return userServicePort.existClient(id);
    }

    @Override
    public Boolean existEmployee(Long id) {
        return userServicePort.existsEmployee(id);
    }


    @Override
    public User getClient(Long id) {
        return null;
    }
}
