<br />
<div align="center">
<h3 align="center">PRAGMA POWER-UP</h3>
  <p align="center">
    In this challenge you are going to design the backend of a system that centralizes the services and orders of a restaurant chain that has different branches in the city.
  </p>
</div>

### Built With

* ![Java](https://img.shields.io/badge/java-%23ED8B00.svg?style=for-the-badge&logo=java&logoColor=white)
* ![Spring](https://img.shields.io/badge/Spring-6DB33F?style=for-the-badge&logo=spring&logoColor=white)
* ![Gradle](https://img.shields.io/badge/Gradle-02303A.svg?style=for-the-badge&logo=Gradle&logoColor=white)
* ![MySQL](https://img.shields.io/badge/MySQL-00000F?style=for-the-badge&logo=mysql&logoColor=white)
* ![MongoDb](https://img.shields.io/badge/MongoDB-3FA037?style=for-the-badge&logo=mongodb&logoColor=white)


<!-- GETTING STARTED -->
## Getting Started

To get a local copy up and running follow these steps.

### Prerequisites

* JDK 17 [https://jdk.java.net/java-se-ri/17](https://jdk.java.net/java-se-ri/17)
* Gradle [https://gradle.org/install/](https://gradle.org/install/)
* MySQL [https://dev.mysql.com/downloads/installer/](https://dev.mysql.com/downloads/installer/)

### Recommended Tools
* IntelliJ Community [https://www.jetbrains.com/idea/download/](https://www.jetbrains.com/idea/download/)
* Postman [https://www.postman.com/downloads/](https://www.postman.com/downloads/)

### Installation

1. Clone the repository
   ```sh
   git clone https://gitlab.com/joelforero5/power-up.git
   ```
   
2. Create a 2 new database in MySQL called "powerup" and "powerup-plazoleta"
   
3. Update bootstrap.yml file in config-server with your configuration with the database connection settings and the repository settings, edit the path to your repository
   ```yml
   # src/main/resources/bootstrap.yml
   server:
      port: 8081
      spring:
        cloud:
          config:
            server:
              git:
                uri: https://gitlab.com/<your-repository>/power-up.git
                default-label: main
                searchPaths: config-data
                username: ${GIT_USER}
                password: ${GIT_PASSWORD}
        security:
        user:
        name: root
        password: s3cr3t
   ```
4. Upload to your repository  the project with the config-data configurations, edit them with your configuration.
   ```yml
    #user-service.yml
      server:
        port: 8090

        spring:
        datasource:
         url: jdbc:mysql://localhost:<<DATABASE_PORT>>/powerup
         username: <<DATABASE_USERNAME>>
         password: <<DATABASE_PASSWORD>>
      jpa:
         hibernate:
         ddl-auto: update
         jwt:
      secret: ${KEYSECRET_SECURITY:eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWUsImlhdCI6MTUxNjIzOTAyMn0.nZU_gPcMXkWpkCUpJceSxS7lSickF0tTImHhAR949Z-Nt69LgW8G6lid-mqd9B579tYM8C4FN2jdhR2VRMsjtA}
      expiration: ${EXPIRE_DAYS_SECURITY:3600000}

      management:
         endpoints:
         web:
         exposure:
         include: "*"

      eureka:
         client:
            serviceUrl:
               defaultZone: http://localhost:8099/eureka/
   ```

5. After the tables are created execute this query content to populate the database
   ```sql
   --POWERUP DATABASE
   --MICRO USUARIOS
   --USUARIOS
   INSERT INTO User (id, nombre, apellido, numero_documento, celular, fecha_nacimiento, correo, clave, role_id)
   VALUES (1, 'Jaime', 'Jaimes', '123', '318844568', '1990-01-01', 'jaime@example.com', '$2a$10$GlsGSNhkbVon6ZOSNMptOu5RikedRzlCAhMa7YpwvUSS0c88WT99S', 1);
   INSERT INTO User (id, nombre, apellido, numero_documento, celular, fecha_nacimiento, correo, clave, role_id)
   VALUES (2, 'Juan', 'Perez', '123', '318844567', '1990-01-01', 'juan@example.com', '$2a$10$GlsGSNhkbVon6ZOSNMptOu5RikedRzlCAhMa7YpwvUSS0c88WT99S', 2);
   INSERT INTO User (id, nombre, apellido, numero_documento, celular, fecha_nacimiento, correo, clave, role_id)
   VALUES (3, 'Jose', 'Padilla', '123', '318544567', '1990-01-01', 'jose@example.com', '$2a$10$GlsGSNhkbVon6ZOSNMptOu5RikedRzlCAhMa7YpwvUSS0c88WT99S', 3);
   INSERT INTO User (id, nombre, apellido, numero_documento, celular, fecha_nacimiento, correo, clave, role_id)
   VALUES (4, 'Jesus', 'Aguilar', '123', '314644567', '1990-01-01', 'jesus@example.com', '$2a$10$GlsGSNhkbVon6ZOSNMptOu5RikedRzlCAhMa7YpwvUSS0c88WT99S', 4);
   --ROLE
   INSERT INTO role (`id`, `descripcion`, `nombre`) VALUES ('1', 'ROLE_ADMIN', 'ROLE_ADMIN');
   INSERT INTO role (`id`, `descripcion`, `nombre`) VALUES ('2', 'ROLE_OWNER', 'ROLE_OWNER');
   INSERT INTO role (`id`, `descripcion`, `nombre`) VALUES ('3', 'ROLE_EMPLOYEE', 'ROLE_EMPLOYEE');
   INSERT INTO role (`id`, `descripcion`, `nombre`) VALUES ('4', 'ROLE_CLIENT', 'ROLE_CLIENT');
   
   --POWERUP-PLAZOLETA DATABASE
   --MICRO PLAZOLETA
   --CATEGORIA
   INSERT INTO categoria (id, descripcion, nombre) VALUES ('1', 'ENTRANCE', 'ENTRANCE');
   INSERT INTO categoria (id, descripcion, nombre) VALUES ('2', 'SNAKS', 'SNAKS');
   INSERT INTO categoria (id, descripcion, nombre) VALUES ('3', 'APPETIZERS', 'APPETIZERS');
   INSERT INTO categoria (id, descripcion, nombre) VALUES ('4', 'SOUPS', 'SOUPS');
   INSERT INTO categoria (id, descripcion, nombre) VALUES ('5', 'MAIN COURSES', 'MAIN COURSES');
   INSERT INTO categoria (id, descripcion, nombre) VALUES ('6', 'DESSERTS', 'DESSERTS');
   INSERT INTO categoria (id, descripcion, nombre) VALUES ('7', 'DRINKS', 'DRINKS');
   --PEDIDO_STATUS
   INSERT INTO `pedido_status` (`id`, `description`, `name`) VALUES (1, 'IN PROGRESS', 'IN PROGRESS');
   INSERT INTO `pedido_status` (`id`, `description`, `name`) VALUES (2, 'IN PREPARATION', 'IN PREPARATION');
   INSERT INTO `pedido_status` (`id`, `description`, `name`) VALUES (3, 'PENDING', 'PENDING');
   INSERT INTO `pedido_status` (`id`, `description`, `name`) VALUES (4, 'READY', 'READY');
   INSERT INTO `pedido_status` (`id`, `description`, `name`) VALUES (5, 'FINISHED', 'FINISHED');
   INSERT INTO `pedido_status` (`id`, `description`, `name`) VALUES (6, 'CANCELLED', 'CANCELLED');
   ```
6. To execute the project you must run each microservice as follows: </br>
   6.1 Run ConfigServiceApplication </br>
   6.2 Run RegistryServiceApplication </br>
   6.3 Run UserMicroserviceApplication </br>
   6.4 Run PlazoletaMicroserviceApplication </br>
   6.5 Run TrackingMicroserviceApplication </br>
   6.6 Run MessengerMicroserviceApplication
7. Open Swagger UI and search the /user/ POST endpoint and create the user
<!-- USAGE -->
## Usage

1. Run UserMicroServiceApplication, Open [http://localhost:8090/swagger-ui/index.html#/]
2. Run PlazoletaMicroserviceApplication, Open [http://localhost:8091/swagger-ui/index.html#/]
3. Open Eureka Server [http://localhost:8099](http://localhost:8099) in your web browser

<!-- ROADMAP -->
## Tests
- Right-click the test folder and choose Run tests with coverage
