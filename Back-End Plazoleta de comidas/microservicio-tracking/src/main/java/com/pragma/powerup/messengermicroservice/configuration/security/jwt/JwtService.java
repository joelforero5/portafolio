package com.pragma.powerup.messengermicroservice.configuration.security.jwt;

public interface JwtService {
    Long getIdToken(String token);
}
